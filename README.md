Pokedex
=======

The pokedex application displays pokemons and their details for a specific user.

The pokemon data is retrieved from an [external API](https://pokeapi.co/).

User data is managed by an [internal API](doc/backend.md)

## Additional documentation

For more information about the different parts of the project see:
- the [backend](doc/backend.md) documentation
- the [endpoints](docs/endpoints.md) documentation
- the [frontend](doc/frontend.md) documentation
