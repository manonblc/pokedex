const userModel = require('./src/models/users')
const pokemonsModel = require('./src/models/pokemons')
const seenPokemonsModel = require('./src/models/seenPokemons')
const caughtPokemonsModel = require('./src/models/caughtPokemons')
const { ROLES, SEXS } = require('./src/utils/constants')

const updatedb = async () => {
  await userModel.sync({ force: true })
  await pokemonsModel.sync({ force: true })
  await seenPokemonsModel.sync({ force: true })
  await caughtPokemonsModel.sync({ force: true })
}

const populatedb = async () => {
  const unique = (value, index, self) => {
    return self.indexOf(value) === index
  }

  const password = '123456'
  const NbPokemons = 40
  // Create 2 teachers
  const sebastien = await userModel.create({ name: 'Sebastien', password, role: ROLES.TEACHER, sex: SEXS.MALE })
  const christine = await userModel.create({ name: 'Christine', password, role: ROLES.TEACHER, sex: SEXS.FEMALE })

  // Create 8 students
  // Sebastien students (total 3 students)
  const sacha = await userModel.create({ name: 'Sacha', password, role: ROLES.STUDENT, sex: SEXS.MALE })
  const benjamin = await userModel.create({ name: 'Benjamin', password, role: ROLES.STUDENT, sex: SEXS.MALE })
  const manon = await userModel.create({ name: 'Manon', password, role: ROLES.STUDENT, sex: SEXS.FEMALE })
  await sebastien.addStudents([sacha, benjamin, manon])
  // Christine students (total 4 students)
  const emma = await userModel.create({ name: 'Emma', password, role: ROLES.STUDENT, sex: SEXS.FEMALE })
  const morgane = await userModel.create({ name: 'Morgane', password, role: ROLES.STUDENT, sex: SEXS.FEMALE })
  const enki = await userModel.create({ name: 'Enki', password, role: ROLES.STUDENT, sex: SEXS.MALE })
  const alex = await userModel.create({ name: 'Alex', password, role: ROLES.STUDENT, sex: SEXS.MALE })
  await christine.addStudents([emma, morgane, enki, alex])
  // A student without teacher
  const hugo = await userModel.create({ name: 'Hugo', password, role: ROLES.STUDENT, sex: SEXS.MALE })

  // Create 40 pokemons
  for (const id of Array(NbPokemons).keys()) {
    await pokemonsModel.create({ id })
  }

  // Add seen pokemons
  for (const student of [sacha, benjamin, morgane, alex, hugo]) {
    const pokemons = Array.from({ length: NbPokemons }, () => Math.floor(Math.random() * NbPokemons)).filter(unique)
    await student.addSeenPokemons(pokemons)
  }

  // Add caught pokemons
  for (const student of [sacha, manon, morgane, alex, enki]) {
    const pokemons = Array.from({ length: NbPokemons }, () => Math.floor(Math.random() * NbPokemons)).filter(unique)
    await student.addSeenPokemons(pokemons)
    await student.addCaughtPokemons(pokemons)
  }
}

const main = async () => {
  await updatedb()
  await populatedb()
}

main()
