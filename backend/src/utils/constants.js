const ROLES = {
  STUDENT: 'student',
  TEACHER: 'teacher'
}

const SEXS = {
  MALE: 'male',
  FEMALE: 'female',
  OTHER: 'other'
}

module.exports = { ROLES, SEXS }
