const has = require('has-keys')
const status = require('http-status')
const userModel = require('../models/users')

function capitalizeFirstLetter (string) {
  if (!string.length) return string
  return string[0].toUpperCase() + string.slice(1)
}

const checkParams = (object, keys) => {
  const errors = {}
  for (const key of keys) {
    if (!object || !has(object, [key]) || !object[key]) errors[key] = `Invalid ${key}`
  }
  return errors
}

const checkUserRole = async (res, role, where) => {
  // Get user
  const user = await userModel.findOne({ where })
  if (!user) {
    res.status(status.NOT_FOUND).send()
    return
  }
  // Check if the user has the correct role
  if (user.role !== role) {
    res.status(status.BAD_REQUEST).send({ __all__: `The user ${user.name} is not a ${role}` })
    return
  }
  return user
}

module.exports = { checkParams, checkUserRole, capitalizeFirstLetter }
