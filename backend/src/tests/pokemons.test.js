const request = require('supertest')
const server = require('../server')
const userModel = require('../models/users')
const pokemonModel = require('../models/pokemons')
const middleware = require('../controllers/middleware')
const { ROLES, SEXS } = require('../utils/constants')

const name = 'azerty'
const password = 'Sup3rS3cr3t'
const role = ROLES.STUDENT
const sex = SEXS.OTHER

// Mock the module with the spy
jest.mock('../models/database.js', () => {
  const Sequelize = require('sequelize')
  return new Sequelize({
    dialect: 'sqlite',
    storage: 'tests.sqlite'
  })
})

beforeEach(async () => {
  // Reset the database
  await require('../models/database').sync({ force: true })
})

// Pokemons seen endpoints - GET
test('Get seen pokemons - user not found', async () => {
  const res = await request(server).get('/api/users/1/pokemons/seen').set('x-access-token', middleware.createJWT(1))
  // Check API response
  expect(res.statusCode).toEqual(403)
})

test('Get seen pokemons - user is a student, empty list', async () => {
  // Create a student
  const user = await userModel.create({ name, role: ROLES.STUDENT, sex, password })
  const res = await request(server).get(`/api/users/${user.id}/pokemons/seen`).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body.length).toEqual(0)
})

test('Get seen pokemons - user is a student OK', async () => {
  // Create a student
  const user = await userModel.create({ name, role: ROLES.STUDENT, sex, password })
  const p1 = await pokemonModel.create({ id: 1 })
  const p2 = await pokemonModel.create({ id: 5 })
  await user.addSeenPokemons([p1, p2])
  const res = await request(server).get(`/api/users/${user.id}/pokemons/seen`).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body.length).toEqual(2)
  expect(res.body).toEqual([p1.id, p2.id])
})

test('Get seen pokemons - user is a teacher, empty list', async () => {
  // Create a teacher
  const user = await userModel.create({ name, role: ROLES.TEACHER, sex, password })
  const res = await request(server).get(`/api/users/${user.id}/pokemons/seen`).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body.length).toEqual(0)
})

test('Get seen pokemons - user is a teacher, OK', async () => {
  // Create a teacher with two students and five pokemons
  const user = await userModel.create({ name, role: ROLES.TEACHER, sex, password })
  const p1 = await pokemonModel.create({ id: 1 })
  const p2 = await pokemonModel.create({ id: 2 })
  const p3 = await pokemonModel.create({ id: 3 })
  const p4 = await pokemonModel.create({ id: 4 })
  const p5 = await pokemonModel.create({ id: 5 })
  const st1 = await userModel.create({ name: 'alpha', role: ROLES.STUDENT, sex, password })
  await st1.addSeenPokemons([p1, p3, p5])
  const st2 = await userModel.create({ name: 'beta', role: ROLES.STUDENT, sex, password })
  await st2.addSeenPokemons([p2, p3, p4])
  await user.addStudents([st1, st2])
  const res = await request(server).get(`/api/users/${user.id}/pokemons/seen`).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body.length).toEqual(5)
  expect(res.body).toEqual([p1.id, p2.id, p3.id, p4.id, p5.id])
})

// Pokemons seen endpoints - POST
test('Post seen pokemons - user not found', async () => {
  const res = await request(server).post('/api/users/1/pokemons/seen').set('x-access-token', middleware.createJWT(1))
  // Check API response
  expect(res.statusCode).toEqual(403)
})

test('Post seen pokemons - without data', async () => {
  // Create a student
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}/pokemons/seen`).send().set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ pokemons: 'Invalid pokemons' })
})

test('Post seen pokemons - missing pokemons parameter', async () => {
  // Create a student
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}/pokemons/seen`).send({}).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ pokemons: 'Invalid pokemons' })
})

test('Post seen pokemons - user is a teacher', async () => {
  // Create a teacher
  const user = await userModel.create({ name, role: ROLES.TEACHER, sex, password })
  const res = await request(server).post(`/api/users/${user.id}/pokemons/seen`).send({ pokemons: [1, 2, 3, 4] }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ __all__: `The user ${user.name} is not a student` })
  // Check database
  const pokemons = await pokemonModel.findAll()
  expect(pokemons.length).toEqual(0)
})

test('Post seen pokemons - user is a student', async () => {
  // Create a student and two pokemons
  const user = await userModel.create({ name, role: ROLES.STUDENT, sex, password })
  const p1 = await pokemonModel.create({ id: 1 })
  const p2 = await pokemonModel.create({ id: 2 })
  const p3 = await pokemonModel.create({ id: 3 })
  const p4 = { id: 5 }
  await user.addSeenPokemon(p1)
  const res = await request(server).post(`/api/users/${user.id}/pokemons/seen`).send({ pokemons: [p1.id, p2.id, p4.id] }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body.length).toEqual(3)
  expect(res.body).toEqual([p1.id, p2.id, p4.id])
  // Check database
  const allPokemons = await pokemonModel.findAll()
  const allPokemonIds = allPokemons.map(p => { return p.id })
  allPokemonIds.sort()
  expect(allPokemonIds.length).toEqual(4)
  expect(allPokemonIds).toEqual([p1.id, p2.id, p3.id, p4.id])
  const pokemons = await user.getSeenPokemons()
  expect(pokemons.length).toEqual(3)
  const pokemonIds = pokemons.map(p => { return p.id }).sort()
  expect(pokemonIds).toEqual([p1.id, p2.id, p4.id])
})

// Pokemons caught endpoints - GET
test('Get caught pokemons - user not found', async () => {
  const res = await request(server).get('/api/users/1/pokemons/caught').set('x-access-token', middleware.createJWT(1))
  // Check API response
  expect(res.statusCode).toEqual(403)
})

test('Get caught pokemons - user is a student, empty list', async () => {
  // Create a student
  const user = await userModel.create({ name, role: ROLES.STUDENT, sex, password })
  const res = await request(server).get(`/api/users/${user.id}/pokemons/caught`).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body.length).toEqual(0)
})

test('Get caught pokemons - user is a student OK', async () => {
  // Create a student
  const user = await userModel.create({ name, role: ROLES.STUDENT, sex, password })
  const p1 = await pokemonModel.create({ id: 1 })
  const p2 = await pokemonModel.create({ id: 5 })
  await user.addCaughtPokemons([p1, p2])
  const res = await request(server).get(`/api/users/${user.id}/pokemons/caught`).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body.length).toEqual(2)
  expect(res.body).toEqual([p1.id, p2.id])
})

test('Get caught pokemons - user is a teacher, empty list', async () => {
  // Create a teacher
  const user = await userModel.create({ name, role: ROLES.TEACHER, sex, password })
  const res = await request(server).get(`/api/users/${user.id}/pokemons/caught`).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body.length).toEqual(0)
})

test('Get caught pokemons - user is a teacher, OK', async () => {
  // Create a teacher with two students and five pokemons
  const user = await userModel.create({ name, role: ROLES.TEACHER, sex, password })
  const p1 = await pokemonModel.create({ id: 1 })
  const p2 = await pokemonModel.create({ id: 2 })
  const p3 = await pokemonModel.create({ id: 3 })
  const p4 = await pokemonModel.create({ id: 4 })
  const p5 = await pokemonModel.create({ id: 5 })
  const st1 = await userModel.create({ name: 'alpha', role: ROLES.STUDENT, sex, password })
  await st1.addCaughtPokemons([p1, p3, p5])
  const st2 = await userModel.create({ name: 'beta', role: ROLES.STUDENT, sex, password })
  await st2.addCaughtPokemons([p2, p3, p4])
  await user.addStudents([st1, st2])
  const res = await request(server).get(`/api/users/${user.id}/pokemons/caught`).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body.length).toEqual(5)
  expect(res.body).toEqual([p1.id, p2.id, p3.id, p4.id, p5.id])
})

// Pokemons caught endpoints - POST
test('Post caught pokemons - user not found', async () => {
  const res = await request(server).post('/api/users/1/pokemons/caught').set('x-access-token', middleware.createJWT(1))
  // Check API response
  expect(res.statusCode).toEqual(403)
})

test('Post caught pokemons - without data', async () => {
  // Create a student
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}/pokemons/caught`).send().set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ pokemons: 'Invalid pokemons' })
})

test('Post caught pokemons - missing pokemons parameter', async () => {
  // Create a student
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}/pokemons/caught`).send({}).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ pokemons: 'Invalid pokemons' })
})

test('Post caught pokemons - user is a teacher', async () => {
  // Create a teacher
  const user = await userModel.create({ name, role: ROLES.TEACHER, sex, password })
  const res = await request(server).post(`/api/users/${user.id}/pokemons/caught`).send({ pokemons: [1, 2, 3, 4] }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ __all__: `The user ${user.name} is not a student` })
  // Check database
  const pokemons = await pokemonModel.findAll()
  expect(pokemons.length).toEqual(0)
})

test('Post caught pokemons - user is a student', async () => {
  // Create a student and two pokemons
  const user = await userModel.create({ name, role: ROLES.STUDENT, sex, password })
  const p1 = await pokemonModel.create({ id: 1 })
  const p2 = await pokemonModel.create({ id: 2 })
  const p3 = await pokemonModel.create({ id: 3 })
  const p4 = { id: 5 }
  await user.addSeenPokemon(p1)
  await user.addCaughtPokemon(p1)
  await user.addCaughtPokemon(p2)
  const res = await request(server).post(`/api/users/${user.id}/pokemons/caught`).send({ pokemons: [p1.id, p2.id, p4.id] }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body.length).toEqual(3)
  expect(res.body).toEqual([p1.id, p2.id, p4.id])
  // Check database
  const allPokemons = await pokemonModel.findAll()
  const allPokemonIds = allPokemons.map(p => { return p.id })
  allPokemonIds.sort()
  expect(allPokemonIds.length).toEqual(4)
  expect(allPokemonIds).toEqual([p1.id, p2.id, p3.id, p4.id])
  for (const type of ['Seen', 'Caught']) {
    const pokemons = await user[`get${type}Pokemons`]()
    expect(pokemons.length).toEqual(3)
    const pokemonIds = pokemons.map(p => { return p.id }).sort()
    expect(pokemonIds).toEqual([p1.id, p2.id, p4.id])
  }
})

server.close()
