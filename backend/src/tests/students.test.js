const request = require('supertest')
const server = require('../server')
const userModel = require('../models/users')
const middleware = require('../controllers/middleware')
const { ROLES, SEXS } = require('../utils/constants')

const name = 'azerty'
const password = 'Sup3rS3cr3t'
const role = ROLES.TEACHER
const sex = SEXS.OTHER

// Mock the module with the spy
jest.mock('../models/database.js', () => {
  const Sequelize = require('sequelize')
  return new Sequelize({
    dialect: 'sqlite',
    storage: 'tests.sqlite'
  })
})

beforeEach(async () => {
  // Reset the database
  await require('../models/database').sync({ force: true })
})

// Students endpoints - GET
test('Get students - user not found', async () => {
  const res = await request(server).get('/api/users/1/students').set('x-access-token', middleware.createJWT(1))
  // Check API response
  expect(res.statusCode).toEqual(403)
})

test('Get students - user is not a teacher', async () => {
  // Create a teacher
  const user = await userModel.create({ name, role: ROLES.STUDENT, sex, password })
  const res = await request(server).get(`/api/users/${user.id}/students`).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ __all__: `The user ${user.name} is not a teacher` })
})

test('Get students - empty list', async () => {
  // Create a teacher
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).get(`/api/users/${user.id}/students`).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body.length).toEqual(0)
})

test('Get students - OK', async () => {
  // Create a teacher with two students
  const user = await userModel.create({ name: 'teacher', role: ROLES.TEACHER, sex, password })
  const st1 = await userModel.create({ name: 'alpha', role: ROLES.STUDENT, sex, password })
  const st2 = await userModel.create({ name: 'beta', role: ROLES.STUDENT, sex, password })
  const students = [st1, st2]
  user.addStudents(students)
  const res = await request(server).get(`/api/users/${user.id}/students`).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body.length).toEqual(students.length)
  expect(res.body[0]).toEqual({ id: st1.id, name: st1.name, sex, role: st1.role })
  expect(res.body[1]).toEqual({ id: st2.id, name: st2.name, sex, role: st2.role })
})

// Students endpoints - POST
test('Add student - without data', async () => {
  // Create a teacher
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}/students`).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ name: 'Invalid name' })
  // Check database
  const nbStudents = await user.countStudents()
  expect(nbStudents).toEqual(0)
})

test('Add student - missing name parameter', async () => {
  // Create a teacher
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}/students`).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ name: 'Invalid name' })
  // Check database
  const nbStudents = await user.countStudents()
  expect(nbStudents).toEqual(0)
})

test('Add student - user not found', async () => {
  const res = await request(server).post('/api/users/1/students').send({ name }).set('x-access-token', middleware.createJWT(1))
  // Check API response
  expect(res.statusCode).toEqual(403)
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(0)
})

test('Add student - user is not a teacher', async () => {
  // Create a fake teacher
  const user = await userModel.create({ name, role: ROLES.STUDENT, sex, password })
  const res = await request(server).post(`/api/users/${user.id}/students`).send({ name }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ __all__: `The user ${user.name} is not a teacher` })
  // Check database
  const nbStudents = await user.countStudents()
  expect(nbStudents).toEqual(0)
})

test('Add student - student not found', async () => {
  // Create a teacher
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}/students`).send({ name: 'oops' }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(404)
  // Check database
  const nbStudents = await user.countStudents()
  expect(nbStudents).toEqual(0)
})

test('Add student - student is not a student', async () => {
  // Create a teacher and a fake student
  const user = await userModel.create({ name, role, sex, password })
  const fakeStudent = await userModel.create({ name: 'oops', role: ROLES.TEACHER, sex, password })
  const res = await request(server).post(`/api/users/${user.id}/students`).send({ name: fakeStudent.name }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ __all__: `The user ${fakeStudent.name} is not a student` })
  // Check database
  const nbStudents = await user.countStudents()
  expect(nbStudents).toEqual(0)
})

test('Add student - student already has a teacher', async () => {
  // Create a two teachers and one student
  const user = await userModel.create({ name, role, sex, password })
  const student = await userModel.create({ name: 'a student', role: ROLES.STUDENT, sex, password })
  const otherTeacher = await userModel.create({ name: 'an other teacher', role, sex, password })
  otherTeacher.addStudent(student)
  const res = await request(server).post(`/api/users/${user.id}/students`).send({ name: student.name }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ name: 'This student already has a teacher' })
  // Check database
  const nbStudents = await user.countStudents()
  expect(nbStudents).toEqual(0)
})

test('Add student - OK', async () => {
  // Create a teacher and a student
  const user = await userModel.create({ name, role, sex, password })
  const student = await userModel.create({ name: 'a student', role: ROLES.STUDENT, sex: SEXS.MALE, password })
  user.addStudent(student)
  const newStudent = await userModel.create({ name: 'new student', role: ROLES.STUDENT, sex: SEXS.FEMALE, password })
  const res = await request(server).post(`/api/users/${user.id}/students`).send({ name: newStudent.name }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body.length).toEqual(2)
  expect(res.body[0]).toEqual({ id: student.id, name: student.name, sex: student.sex, role: student.role })
  expect(res.body[1]).toEqual({ id: newStudent.id, name: newStudent.name, sex: newStudent.sex, role: student.role })
  // Check database
  const nbStudents = await user.countStudents()
  expect(nbStudents).toEqual(2)
  const ids = (await user.getStudents()).map(s => s.id)
  expect(ids).toEqual([student.id, newStudent.id])
})

// Students endpoints - DELETE
test('Remove student - without data', async () => {
  // Create a teacher
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).delete(`/api/users/${user.id}/students`).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ name: 'Invalid name' })
  // Check database
  const nbStudents = await user.countStudents()
  expect(nbStudents).toEqual(0)
})

test('Remove student - missing name parameter', async () => {
  // Create a teacher
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).delete(`/api/users/${user.id}/students`).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ name: 'Invalid name' })
  // Check database
  const nbStudents = await user.countStudents()
  expect(nbStudents).toEqual(0)
})

test('Remove student - user not found', async () => {
  const res = await request(server).delete('/api/users/1/students').send({ name }).set('x-access-token', middleware.createJWT(1))
  // Check API response
  expect(res.statusCode).toEqual(403)
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(0)
})

test('Remove student - user is not a teacher', async () => {
  // Create a fake teacher
  const user = await userModel.create({ name, role: ROLES.STUDENT, sex, password })
  const res = await request(server).delete(`/api/users/${user.id}/students`).send({ name }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ __all__: `The user ${user.name} is not a teacher` })
  // Check database
  const nbStudents = await user.countStudents()
  expect(nbStudents).toEqual(0)
})

test('Remove student - student not found', async () => {
  // Create a teacher
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).delete(`/api/users/${user.id}/students`).send({ name: 'oops' }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(404)
  // Check database
  const nbStudents = await user.countStudents()
  expect(nbStudents).toEqual(0)
})

test('Remove student - student is not a student', async () => {
  // Create a teacher and a fake student
  const user = await userModel.create({ name, role, sex, password })
  const fakeStudent = await userModel.create({ name: 'oops', role: ROLES.TEACHER, sex, password })
  const res = await request(server).delete(`/api/users/${user.id}/students`).send({ name: fakeStudent.name }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ __all__: `The user ${fakeStudent.name} is not a student` })
  // Check database
  const nbStudents = await user.countStudents()
  expect(nbStudents).toEqual(0)
})

test('Remove student - student is not in the student list', async () => {
  // Create a teacher and a student
  const user = await userModel.create({ name, role, sex, password })
  const student = await userModel.create({ name: 'a student', role: ROLES.STUDENT, sex, password })
  const res = await request(server).delete(`/api/users/${user.id}/students`).send({ name: student.name }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(404)
  // Check database
  const nbStudents = await user.countStudents()
  expect(nbStudents).toEqual(0)
})

test('Remove student - OK', async () => {
  // Create a teacher and two students
  const user = await userModel.create({ name, role, sex, password })
  const student = await userModel.create({ name: 'a student', role: ROLES.STUDENT, sex, password })
  const oldStudent = await userModel.create({ name: 'new student', role: ROLES.STUDENT, sex, password })
  user.addStudents([student, oldStudent])
  const res = await request(server).delete(`/api/users/${user.id}/students`).send({ name: oldStudent.name }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body.length).toEqual(1)
  expect(res.body[0]).toEqual({ id: student.id, name: student.name, sex, role: ROLES.STUDENT })
  // Check database
  const nbStudents = await user.countStudents()
  expect(nbStudents).toEqual(1)
  const ids = (await user.getStudents()).map(s => s.id)
  expect(ids).toEqual([student.id])
})

server.close()
