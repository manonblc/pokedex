const request = require('supertest')
const bcrypt = require('bcrypt')
const server = require('../server')
const userModel = require('../models/users')
const middleware = require('../controllers/middleware')
const { ROLES, SEXS } = require('../utils/constants')

const name = 'azerty'
const password = 'Sup3rS3cr3t'
const role = ROLES.TEACHER
const sex = SEXS.OTHER

// Mock the module with the spy
jest.mock('../models/database.js', () => {
  const Sequelize = require('sequelize')
  return new Sequelize({
    dialect: 'sqlite',
    storage: 'tests.sqlite'
  })
})

beforeEach(async () => {
  // Reset the database
  await require('../models/database').sync({ force: true })
})

// Login endpoint - GET
test('Loggin - without data', async () => {
  const res = await request(server).post('/api/users/login')
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ name: 'Invalid name', password: 'Invalid password' })
})

test('Loggin - missing all parameters', async () => {
  const res = await request(server).post('/api/users/login').send({})
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ name: 'Invalid name', password: 'Invalid password' })
})

test('Loggin - missing name parameter', async () => {
  const res = await request(server).post('/api/users/login').send({ password })
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ name: 'Invalid name' })
})

test('Loggin - missing password parameter', async () => {
  const res = await request(server).post('/api/users/login').send({ name })
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ password: 'Invalid password' })
})

test('Login - user not exists', async () => {
  const res = await request(server).post('/api/users/login').send({ name, password })
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ __all__: 'Invalid login or password' })
})

test('Login - OK', async () => {
  // Create one user
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post('/api/users/login').send({ name, password })
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body).toEqual({ id: user.id, token: middleware.createJWT(user.id) })
})

// Users endpoint - POST
test('New user - without data', async () => {
  const res = await request(server).post('/api/users')
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ name: 'Invalid name', password: 'Invalid password', role: 'Invalid role', sex: 'Invalid sex' })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(0)
})

test('New user - missing all parameters', async () => {
  const res = await request(server).post('/api/users').send({})
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ name: 'Invalid name', password: 'Invalid password', role: 'Invalid role', sex: 'Invalid sex' })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(0)
})

test('New user - missing name parameter', async () => {
  const res = await request(server).post('/api/users').send({ role, sex, password })
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ name: 'Invalid name' })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(0)
})

test('New user - missing password parameter', async () => {
  const res = await request(server).post('/api/users').send({ name, role, sex })
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ password: 'Invalid password' })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(0)
})

test('New user - missing role parameter', async () => {
  const res = await request(server).post('/api/users').send({ name, sex, password })
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ role: 'Invalid role' })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(0)
})

test('New user - missing sex parameter', async () => {
  const res = await request(server).post('/api/users').send({ name, role, password })
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ sex: 'Invalid sex' })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(0)
})

test('New user - name too long, role is invalid and sex is invalid', async () => {
  const res = await request(server).post('/api/users').send({ name: 'azertyopentyaventi', role: 'my-role', sex: 'my-sex', password })
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ name: 'Name too long', role: 'Invalid role', sex: 'Invalid sex' })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(0)
})

test('New user - name too long', async () => {
  const res = await request(server).post('/api/users').send({ name: 'azertyopentyaventi', role, sex, password })
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ name: 'Name too long' })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(0)
})

test('New user - role is invalid', async () => {
  const res = await request(server).post('/api/users').send({ name, role: 'my-role', sex, password })
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ role: 'Invalid role' })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(0)
})

test('New user - sex is invalid', async () => {
  const res = await request(server).post('/api/users').send({ name, role, sex: 'my-sex', password })
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ sex: 'Invalid sex' })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(0)
})

test('New user - name already exists', async () => {
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post('/api/users').send({ name, role, sex, password })
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ name: 'Name already used' })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(1)
  const userDb = users.pop()
  expect(userDb.id).toEqual(user.id)
  expect(userDb.name).toEqual(name)
  expect(userDb.role).toEqual(role)
  expect(userDb.sex).toEqual(sex)
  expect(bcrypt.compareSync(password, user.password)).toBe(true)
})

test('New user - OK', async () => {
  const id = 1
  const res = await request(server).post('/api/users').send({ name, role, sex, password })
  // Check API response
  expect(res.statusCode).toEqual(201)
  expect(res.body).toEqual({ id, name, role, sex })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(1)
  const user = users.pop()
  expect(user.id).toEqual(id)
  expect(user.name).toEqual(name)
  expect(user.role).toEqual(role)
  expect(user.sex).toEqual(sex)
  expect(bcrypt.compareSync(password, user.password)).toBe(true)
})

// User endpoints - GET
test('Get user - with teacher', async () => {
  // Create two users
  const teacher = await userModel.create({ name: 'alpha', role: ROLES.TEACHER, sex: SEXS.MALE, password })
  const user = await userModel.create({ name: 'beta', role: ROLES.STUDENT, sex: SEXS.FEMALE, password })
  teacher.addStudents(user)
  const res = await request(server).get(`/api/users/${user.id}`).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body).toEqual({
    id: user.id,
    name: user.name,
    role: user.role,
    sex: user.sex,
    teacher: {
      id: teacher.id,
      name: teacher.name,
      role: teacher.role,
      sex: teacher.sex
    }
  })
})

test('Get user - not found', async () => {
  const res = await request(server).get('/api/users/1')
  // Check API response
  expect(res.statusCode).toEqual(403)
})

// User endpoints - POST
test('Update user - without data', async () => {
  // Create one user
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}`).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ password: 'Invalid password' })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(1)
  const userDb = users.pop()
  expect(userDb.id).toEqual(user.id)
  expect(userDb.name).toEqual(name)
  expect(userDb.role).toEqual(role)
  expect(userDb.sex).toEqual(sex)
  expect(bcrypt.compareSync(password, userDb.password)).toBe(true)
})

test('Update user - not exists', async () => {
  const res = await request(server).post('/api/users/1').send({ password })
  // Check API response
  expect(res.statusCode).toEqual(403)
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(0)
})

test('Update user - name', async () => {
  const newName = 'alpha'
  // Create one user
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}`).send({ name: newName, password }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body).toEqual({ id: user.id, name: newName, role, sex, teacher: null })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(1)
  const userDb = users.pop()
  expect(userDb.id).toEqual(user.id)
  expect(userDb.name).toEqual(newName)
  expect(userDb.role).toEqual(role)
  expect(userDb.sex).toEqual(sex)
  expect(bcrypt.compareSync(password, userDb.password)).toBe(true)
})

test('Update user - password', async () => {
  const newPassword = 'LessSecret'
  // Create one user
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}`).send({ password, newPassword }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body).toEqual({ id: user.id, name, role, sex, teacher: null })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(1)
  const userDb = users.pop()
  expect(userDb.id).toEqual(user.id)
  expect(userDb.name).toEqual(name)
  expect(userDb.role).toEqual(role)
  expect(userDb.sex).toEqual(sex)
  expect(bcrypt.compareSync(newPassword, userDb.password)).toBe(true)
})

test('Update user - sex', async () => {
  const newSex = SEXS.FEMALE
  // Create one user
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}`).send({ sex: newSex, password }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body).toEqual({ id: user.id, name, role, sex: newSex, teacher: null })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(1)
  const userDb = users.pop()
  expect(userDb.id).toEqual(user.id)
  expect(userDb.name).toEqual(name)
  expect(userDb.role).toEqual(role)
  expect(userDb.sex).toEqual(newSex)
  expect(bcrypt.compareSync(password, userDb.password)).toBe(true)
})

test('Update user - name and role', async () => {
  const newName = 'alpha'
  // Create one user
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}`).send({ name: newName, role: 'my-role', password }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body).toEqual({ id: user.id, name: newName, role, sex, teacher: null })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(1)
  const userDb = users.pop()
  expect(userDb.id).toEqual(user.id)
  expect(userDb.name).toEqual(newName)
  expect(userDb.role).toEqual(role)
  expect(userDb.sex).toEqual(sex)
  expect(bcrypt.compareSync(password, userDb.password)).toBe(true)
})

test('Update user - name, role and sex', async () => {
  const newName = 'alpha'
  const newSex = SEXS.FEMALE
  // Create one user
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}`).send({ name: newName, role: 'my-role', sex: newSex, password }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(200)
  expect(res.body).toEqual({ id: user.id, name: newName, role, sex: newSex, teacher: null })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(1)
  const userDb = users.pop()
  expect(userDb.id).toEqual(user.id)
  expect(userDb.name).toEqual(newName)
  expect(userDb.role).toEqual(role)
  expect(userDb.sex).toEqual(newSex)
  expect(bcrypt.compareSync(password, userDb.password)).toBe(true)
})

test('Update user - name too long, invalid sex and invalid password', async () => {
  // Create one user
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}`).send({ password: 'oops', name: 'A new name very very long', sex: 'my-sex' }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ name: 'Name too long', sex: 'Invalid sex', password: 'Invalid password' })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(1)
  const userDb = users.pop()
  expect(userDb.id).toEqual(user.id)
  expect(userDb.name).toEqual(name)
  expect(userDb.role).toEqual(role)
  expect(userDb.sex).toEqual(sex)
  expect(bcrypt.compareSync(password, userDb.password)).toBe(true)
})

test('Update user - invalid sex', async () => {
  // Create one user
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}`).send({ password, sex: 'my-sex' }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ sex: 'Invalid sex' })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(1)
  const userDb = users.pop()
  expect(userDb.id).toEqual(user.id)
  expect(userDb.name).toEqual(name)
  expect(userDb.role).toEqual(role)
  expect(userDb.sex).toEqual(sex)
  expect(bcrypt.compareSync(password, userDb.password)).toBe(true)
})

test('Update user - invalid password', async () => {
  // Create one user
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}`).send({ password: 'oops' }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ password: 'Invalid password' })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(1)
  const userDb = users.pop()
  expect(userDb.id).toEqual(user.id)
  expect(userDb.name).toEqual(name)
  expect(userDb.role).toEqual(role)
  expect(userDb.sex).toEqual(sex)
  expect(bcrypt.compareSync(password, userDb.password)).toBe(true)
})

test('Update user - name too long', async () => {
  // Create one user
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}`).send({ password, name: 'A new name very very long' }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ name: 'Name too long' })
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(1)
  const userDb = users.pop()
  expect(userDb.id).toEqual(user.id)
  expect(userDb.name).toEqual(name)
  expect(userDb.role).toEqual(role)
  expect(userDb.sex).toEqual(sex)
  expect(bcrypt.compareSync(password, userDb.password)).toBe(true)
})

test('Update user - name already used', async () => {
  // Create one user
  const otherName = 'other name'
  const user = await userModel.create({ name, role, sex, password })
  const otherUser = await userModel.create({ name: otherName, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}`).send({ password, name: otherUser.name }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(400)
  expect(res.body).toEqual({ name: 'Name already used' })
  // Check database
  let userDb = await userModel.findOne({ where: { name } })
  expect(userDb).not.toBeNull()
  expect(userDb.id).toEqual(user.id)
  expect(userDb.name).toEqual(name)
  expect(userDb.role).toEqual(role)
  expect(userDb.sex).toEqual(sex)
  expect(bcrypt.compareSync(password, userDb.password)).toBe(true)
  userDb = await userModel.findOne({ where: { name: otherName } })
  expect(userDb).not.toBeNull()
  expect(userDb.id).toEqual(otherUser.id)
  expect(userDb.name).toEqual(otherUser.name)
  expect(userDb.role).toEqual(role)
  expect(userDb.sex).toEqual(sex)
  expect(bcrypt.compareSync(password, userDb.password)).toBe(true)
})

test('Update user - nothing', async () => {
  // Create one user
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).post(`/api/users/${user.id}`).send({ password }).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(304)
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(1)
  const userDb = users.pop()
  expect(userDb.id).toEqual(user.id)
  expect(userDb.name).toEqual(name)
  expect(userDb.role).toEqual(role)
  expect(userDb.sex).toEqual(sex)
  expect(bcrypt.compareSync(password, userDb.password)).toBe(true)
})

// User endpoints - DELETE
test('Delete user - not exists', async () => {
  const res = await request(server).delete('/api/users/1')
  // Check API response
  expect(res.statusCode).toEqual(403)
  expect(res.body).toEqual('')
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(0)
})

test('Delete user - OK', async () => {
  const user = await userModel.create({ name, role, sex, password })
  const res = await request(server).delete(`/api/users/${user.id}`).set('x-access-token', middleware.createJWT(user.id))
  // Check API response
  expect(res.statusCode).toEqual(204)
  expect(res.body).toEqual({})
  // Check database
  const users = await userModel.findAll()
  expect(users.length).toEqual(0)
})

server.close()
