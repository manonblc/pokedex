const express = require('express')
const request = require('supertest')
const status = require('http-status')
const userModel = require('../models/users')
const middleware = require('../controllers/middleware')
const { ROLES, SEXS } = require('../utils/constants')

const id = 1
const name = 'azerty'
const password = 'Sup3rS3cr3t'
const role = ROLES.TEACHER
const sex = SEXS.OTHER
const token = middleware.createJWT(id)

// Create a fake endpoint
const { PORT } = process.env
const fakeApp = express()
fakeApp.use('/new-endpoint/:id', middleware.checkJWT)
fakeApp.get('/new-endpoint/:id', (req, res) => { res.status(status.OK).send() })
const fakeServer = fakeApp.listen(
  PORT,
  () => console.info('Server listening on port ', PORT)
)

// Mock the module with the spy
jest.mock('../models/database.js', () => {
  const Sequelize = require('sequelize')
  return new Sequelize({
    dialect: 'sqlite',
    storage: 'tests.sqlite'
  })
})

beforeEach(async () => {
  // Reset the database
  await require('../models/database').sync({ force: true })
})

// Create JWT
test('Create JWT', async () => {
  expect(token).not.toBeNull()
  expect(token.split('.').length).toEqual(3)
})

// Decode JWT
test('Decode JWT', async () => {
  expect(middleware.decodeJwt()).toBeUndefined()
  expect(middleware.decodeJwt('test.token')).toBeUndefined()
  expect(middleware.decodeJwt(token)).toEqual(String(id))
})

// Check JWT
test('Check JWT - wrong token', async () => {
  // Create one user
  await userModel.create({ id, name, role, sex, password })
  // Create a fake app with a fake endpoint
  const res = await request(fakeServer).get(`/new-endpoint/${id}`).set('x-access-token', 'test.token')
  // Check API response
  expect(res.statusCode).toEqual(403)
})

test('Check JWT - user not exists', async () => {
  const res = await request(fakeServer).get(`/new-endpoint/${id}`).set('x-access-token', token)
  // Check API response
  expect(res.statusCode).toEqual(403)
})

test('Check JWT - wrong query param', async () => {
  const res = await request(fakeServer).get(`/new-endpoint/${id + 1}`).set('x-access-token', token)
  // Check API response
  expect(res.statusCode).toEqual(403)
})

test('Check JWT - OK', async () => {
  // Create one user
  await userModel.create({ id, name, role, sex, password })
  const res = await request(fakeServer).get(`/new-endpoint/${id}`).set('x-access-token', token)
  // Check API response
  expect(res.statusCode).toEqual(200)
})

fakeServer.close()
