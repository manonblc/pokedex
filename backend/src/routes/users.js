const express = require('express')
const router = express.Router()
const middleware = require('../controllers/middleware')
const users = require('../controllers/users')
const students = require('../controllers/students')
const pokemons = require('../controllers/pokemons')

// Login
router.post('/login', users.login)

// Users
router.post('/', users.create)

router.use('/:id', middleware.checkJWT)

// User
router.get('/:id', users.get)
router.post('/:id', users.update)
router.delete('/:id', users.delete)

// Students
router.get('/:id/students', students.get)
router.post('/:id/students', students.add)
router.delete('/:id/students', students.remove)

// Pokemons
router.get('/:id/pokemons/seen', pokemons.getSeen)
router.post('/:id/pokemons/seen', pokemons.addSeen)

router.get('/:id/pokemons/caught', pokemons.getCaught)
router.post('/:id/pokemons/caught', pokemons.addCaught)

module.exports = router
