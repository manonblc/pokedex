const db = require('./database')

const seenPokemons = db.define('seenPokemons', {}, { timestamps: false })

module.exports = seenPokemons
