const Sequelize = require('sequelize')

const db = new Sequelize({
  dialect: 'sqlite',
  storage: 'pokedex.sqlite',
  logging: (...msg) => {}
})

module.exports = db
