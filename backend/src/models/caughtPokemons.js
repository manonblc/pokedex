const db = require('./database')

const caughtPokemons = db.define('caughtPokemons', {}, { timestamps: false })

module.exports = caughtPokemons
