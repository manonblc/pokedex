const Sequelize = require('sequelize')
const db = require('./database')
const users = require('./users')
const seenPokemons = require('./seenPokemons')
const caughtPokemons = require('./caughtPokemons')

const pokemons = db.define('pokemons', {
  id: {
    primaryKey: true,
    type: Sequelize.INTEGER
  }
}, { timestamps: false })

pokemons.belongsToMany(users, { through: seenPokemons })
users.belongsToMany(pokemons, { as: 'SeenPokemons', through: seenPokemons })

pokemons.belongsToMany(users, { through: caughtPokemons })
users.belongsToMany(pokemons, { as: 'CaughtPokemons', through: caughtPokemons })

module.exports = pokemons
