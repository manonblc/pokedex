const Sequelize = require('sequelize')
const bcrypt = require('bcrypt')
const db = require('./database')
const { ROLES, SEXS } = require('../utils/constants')

const users = db.define('users', {
  id: {
    primaryKey: true,
    type: Sequelize.INTEGER,
    autoIncrement: true
  },
  name: {
    type: Sequelize.STRING(16),
    allowNull: false,
    unique: true
  },
  role: {
    type: Sequelize.ENUM(Object.values(ROLES)),
    allowNull: false
  },
  sex: {
    type: Sequelize.ENUM(Object.values(SEXS)),
    allowNull: false
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false
  }
}, { timestamps: false })

// Password management
// Hash the password in the database
const generateHash = (user, options) => {
  if (!user || !user.password) return
  if (!user.changed('password')) return user.password
  const salt = bcrypt.genSaltSync()
  user.password = bcrypt.hashSync(user.password, salt)
}

users.beforeCreate(generateHash)
users.beforeUpdate(generateHash)

// Do not return the password in the response
users.prototype.toJSON = function () {
  const values = Object.assign({}, this.get())
  delete values.password
  return values
}

users.hasMany(users, { as: 'students', foreignKey: 'teacherId' })
users.belongsTo(users, { as: 'teacher', foreignKey: 'teacherId' })

module.exports = users
