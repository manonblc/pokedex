const status = require('http-status')
const jws = require('jws')
const userModel = require('../models/users')

const { SECRET, ALGO } = process.env

const createJWT = (id) => {
  return jws.sign({
    header: { alg: ALGO },
    payload: id,
    secret: SECRET
  })
}

const decodeJwt = (token) => {
  /*
  * Check the number of dots because
  * if the token have not two dots
  * jws.verify() raise an error
  */
  if (!token || token.split('.').length !== 3 || !jws.verify(token, ALGO, SECRET)) return
  const data = jws.decode(token)
  return data.payload
}

const checkJWT = async (req, res, next) => {
  const token = req.get('x-access-token')
  const idToken = decodeJwt(token)
  const { id } = req.params
  if (idToken !== id) {
    res.status(status.FORBIDDEN).send()
  } else {
    const user = await userModel.findOne({ where: { id } })
    if (!user) res.status(status.FORBIDDEN).send()
    else next()
  }
}

module.exports = { createJWT, decodeJwt, checkJWT }
