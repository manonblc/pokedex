const status = require('http-status')
const { ROLES } = require('../utils/constants')
const helpers = require('../utils/helpers')

module.exports = {
  async get (req, res) {
    // Get teacher
    const { id } = req.params
    const teacher = await helpers.checkUserRole(res, ROLES.TEACHER, { id })
    if (!teacher) return
    // Get students
    const students = await teacher.getStudents({ attributes: ['id', 'name', 'sex', 'role'] })
    res.status(status.OK).send(students)
  },
  async add (req, res) {
    // Check body keys
    const errors = helpers.checkParams(req.body, ['name'])
    if (Object.keys(errors).length) {
      res.status(status.BAD_REQUEST).send(errors)
      return
    }
    // Get teacher
    const { id } = req.params
    const teacher = await helpers.checkUserRole(res, ROLES.TEACHER, { id })
    if (!teacher) return
    // Get student
    const { name } = req.body
    const student = await helpers.checkUserRole(res, ROLES.STUDENT, { name })
    if (!student) return
    // Check if the student already has a teacher
    if (student.teacherId) {
      res.status(status.BAD_REQUEST).send({ name: 'This student already has a teacher' })
      return
    }
    // Add student
    await teacher.addStudent(student)
    const data = await teacher.getStudents({ attributes: ['id', 'name', 'sex', 'role'] })
    res.status(status.OK).send(data)
  },
  async remove (req, res) {
    // Check body keys
    const errors = helpers.checkParams(req.body, ['name'])
    if (Object.keys(errors).length) {
      res.status(status.BAD_REQUEST).send(errors)
      return
    }
    // Get teacher
    const { id } = req.params
    const teacher = await helpers.checkUserRole(res, ROLES.TEACHER, { id })
    if (!teacher) return
    // Get student
    const { name } = req.body
    const student = await helpers.checkUserRole(res, ROLES.STUDENT, { name })
    if (!student) return
    // Check if the teacher has this student
    const hasStudent = await teacher.hasStudent(student)
    if (!hasStudent) {
      res.status(status.NOT_FOUND).send()
      return
    }
    // Remove student
    await teacher.removeStudent(student)
    const data = await teacher.getStudents({ attributes: ['id', 'name', 'sex', 'role'] })
    res.status(status.OK).send(data)
  }
}
