const status = require('http-status')
const bcrypt = require('bcrypt')
const middleware = require('../controllers/middleware')
const userModel = require('../models/users')
const helpers = require('../utils/helpers')
const { ROLES, SEXS } = require('../utils/constants')

module.exports = {
  async login (req, res) {
    // Check query keys
    const errors = helpers.checkParams(req.body, ['name', 'password'])
    if (Object.keys(errors).length) {
      res.status(status.BAD_REQUEST).send(errors)
      return
    }
    // Get user
    const { name, password } = req.body
    const user = await userModel.findOne({
      where: { name },
      attributes: ['id', 'name', 'role', 'sex', 'password'],
      include: {
        model: userModel,
        attributes: ['id', 'name', 'role', 'sex'],
        as: 'teacher'
      }
    })
    if (!user || !bcrypt.compareSync(password, user.password)) {
      res.status(status.BAD_REQUEST).send({ __all__: 'Invalid login or password' })
      return
    }
    res.status(status.OK).send({ id: user.id, token: middleware.createJWT(user.id) })
  },
  async create (req, res) {
    // Check body keys
    let errors = helpers.checkParams(req.body, ['name', 'role', 'sex', 'password'])
    if (Object.keys(errors).length) {
      res.status(status.BAD_REQUEST).send(errors)
      return
    }
    // Check parameters
    const { name, role, sex, password } = req.body
    if (!Object.values(ROLES).includes(role)) errors.role = 'Invalid role'
    if (!Object.values(SEXS).includes(sex)) errors.sex = 'Invalid sex'
    if (name.length > 16) errors.name = 'Name too long'
    if (Object.keys(errors).length) {
      res.status(status.BAD_REQUEST).send(errors)
      return
    }
    // Create user
    try {
      const user = await userModel.create(
        { name, role, password, sex },
        { fields: ['id', 'name', 'role', 'sex', 'password'] }
      )
      res.status(status.CREATED).send(user)
    } catch (error) {
      errors = error.errors.reduce((acc, error) => {
        if (error.message === 'name must be unique') acc.name = 'Name already used'
        return acc
      }, {})
      const state = Object.keys(errors).length ? status.BAD_REQUEST : status.INTERNAL_SERVER_ERROR
      if (state === status.INTERNAL_SERVER_ERROR) errors = error
      res.status(state).send(errors)
    }
  },
  async get (req, res) {
    // Get user
    const { id } = req.params
    const user = await userModel.findOne({
      where: { id },
      attributes: ['id', 'name', 'role', 'sex'],
      include: {
        model: userModel,
        attributes: ['id', 'name', 'role', 'sex'],
        as: 'teacher'
      }
    })
    res.status(status.OK).send(user)
  },
  async update (req, res) {
    // Check body keys
    let errors = helpers.checkParams(req.body, ['password'])
    if (Object.keys(errors).length) {
      res.status(status.BAD_REQUEST).send(errors)
      return
    }
    const { name, sex, password, newPassword } = req.body
    // Get user
    const { id } = req.params
    const user = await userModel.findOne({
      where: { id },
      attributes: ['id', 'name', 'role', 'sex', 'password'],
      include: {
        model: userModel,
        attributes: ['id', 'name', 'role', 'sex'],
        as: 'teacher'
      }
    })
    // Check password
    if (!bcrypt.compareSync(password, user.password)) errors.password = 'Invalid password'
    // Check parameters
    if (sex && !Object.values(SEXS).includes(sex)) errors.sex = 'Invalid sex'
    if (name && name.length > 16) errors.name = 'Name too long'
    if (Object.keys(errors).length) {
      res.status(status.BAD_REQUEST).send(errors)
      return
    }
    // Update user
    user.name = name || user.name
    user.sex = sex || user.sex
    user.password = newPassword || user.password
    if (!user.changed()) {
      res.status(status.NOT_MODIFIED).send()
      return
    }
    // Save user
    if (user.changed()) {
      try {
        await user.save()
      } catch (error) {
        errors = error.errors.reduce((acc, error) => {
          if (error.message === 'name must be unique') acc.name = 'Name already used'
          return acc
        }, {})
        const state = Object.keys(errors).length ? status.BAD_REQUEST : status.INTERNAL_SERVER_ERROR
        if (state === status.INTERNAL_SERVER_ERROR) errors = error
        res.status(state).send(errors)
      }
    }
    res.status(status.OK).send(user)
  },
  async delete (req, res) {
    // Delete user
    const { id } = req.params
    await userModel.destroy({ where: { id } })
    res.status(status.NO_CONTENT).send()
  }
}
