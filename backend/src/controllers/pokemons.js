const status = require('http-status')
const { ROLES } = require('../utils/constants')
const helpers = require('../utils/helpers')
const userModel = require('../models/users')
const pokemonModel = require('../models/pokemons')

const getPokemons = async (students, type) => {
  const method = `get${helpers.capitalizeFirstLetter(type)}Pokemons`
  // List all pokemons for each student
  const allPokemons = new Set()
  for (const student of students) {
    const pokemons = await student[method]()
    // Get a distinct list of pokemon IDs
    pokemons.forEach(p => allPokemons.add(p.id))
  }
  return [...allPokemons].sort()
}

module.exports = {
  async getSeen (req, res) {
    // Get user
    const { id } = req.params
    const user = await userModel.findOne({
      where: { id },
      attributes: ['id', 'role']
    })
    // Get all seen pokemons
    const students = user.role === ROLES.TEACHER ? await user.getStudents({ attributes: ['id'] }) : [user]
    const pokemons = await getPokemons(students, 'seen')
    res.status(status.OK).send(pokemons)
  },
  async addSeen (req, res) {
    // Check body keys
    const errors = helpers.checkParams(req.body, ['pokemons'])
    if (Object.keys(errors).length) {
      res.status(status.BAD_REQUEST).send(errors)
      return
    }
    const { pokemons } = req.body
    // Get student
    const { id } = req.params
    const student = await helpers.checkUserRole(res, ROLES.STUDENT, { id })
    if (!student) return
    // Create pokemons if they not exist
    for (const pokemonId of pokemons) {
      const pokemon = await pokemonModel.findOne({ where: { id: pokemonId } })
      if (!pokemon) await pokemonModel.create({ id: pokemonId })
    }
    // Add pokemons
    await student.addSeenPokemons(pokemons)
    const seenPokemons = await getPokemons([student], 'seen')
    res.status(status.OK).send(seenPokemons)
  },
  async getCaught (req, res) {
    // Get user
    const { id } = req.params
    const user = await userModel.findOne({
      where: { id },
      attributes: ['id', 'role']
    })
    // Get all caught pokemons
    const students = user.role === ROLES.TEACHER ? await user.getStudents({ attributes: ['id'] }) : [user]
    const pokemons = await getPokemons(students, 'caught')
    res.status(status.OK).send(pokemons)
  },
  async addCaught (req, res) {
    // Check body keys
    const errors = helpers.checkParams(req.body, ['pokemons'])
    if (Object.keys(errors).length) {
      res.status(status.BAD_REQUEST).send(errors)
      return
    }
    const { pokemons } = req.body
    // Get student
    const { id } = req.params
    const student = await helpers.checkUserRole(res, ROLES.STUDENT, { id })
    if (!student) return
    // Create pokemons if they not exist
    for (const pokemonId of pokemons) {
      const pokemon = await pokemonModel.findOne({ where: { id: pokemonId } })
      if (!pokemon) await pokemonModel.create({ id: pokemonId })
    }
    // Add pokemons
    // A caught pokemon is automatically seen
    await student.addSeenPokemons(pokemons)
    await student.addCaughtPokemons(pokemons)
    const caughtPokemons = await getPokemons([student], 'caught')
    res.status(status.OK).send(caughtPokemons)
  }
}
