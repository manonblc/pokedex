# Frontend

The frontend allows to make the link between the user and the APIs used.

With the help of the different APIs, the frontend allows to have a mobile application that displays the pokemons seen or caught for a specific user.

**Note**: For the rest of the documentation we will consider the commands in the `frontend` folder.

## Demo

You can find a short video showing the application in different use case in the "doc" folder or following this link : https://drive.google.com/file/d/1_VlkM2sGeDhnCU8bnwwe5LClb-oYDBL0/view?usp=sharing

## Architecture

In the `frontend` folder is:
- an `index.js` file which is the entry point to launch the application
- an `app.json` file which determines the different parameters of the application (like the name)
- a `.eslintrc.yml` and a `.prettierrc.js` file that allows to define the different rules of linting
- an `android` folder that allows to build the android application
- an `ios` folder that allows to build the android application
- a `js` folder
- a `components` folder which contains the different components used by the application
- an `img` folder which contains the images used by the application. It contains only the avatar of the user displayed according to their role (`student` or `teacher`) and their gender (`male` or `female`)

### Components folder

In the `components` folder is:
- an `App` component which is the main component of the application
- a `Register` component which is the component used to register a user
- a `Login` component which is the component used to log in
- a `Header` component which is the component displaying the different sections of the application (`Pokemons`, `Infos` (for students), `Students` (for teachers), `Profile`)
- a `Pokemons` component which is the component displaying the list of pokemons
- a `DetailPokemon` component which is the component displaying the details of a pokemon
- an `Infos` component which is the component displaying information about a student (his teacher and the number of pokemons seen and caught)
- a `Students` component which is the component allowing the management of students for a teacher
- a `Profile` component which is the component allowing to modify his profile, to delete his account or to log out
- a `User` component which displays a user
- a `TypesIcons` component which displays the types of a pokemon

### Js folder

In the `js` folder is :
- an `api.js` file which contains the different methods to call [our backend](./backend.md)
- a `pokeapi.js` file which contains the different methods to call the [pokeapi](https://pokeapi.co)
- a `styles.js` file which contains the generic css styles used by several components
- a `constants.js` file which contains the different constants of the application

## Installation

To install the frontend, run the following commands:
```
npm install
```

## Start a the application

To start and install the application on your phone, you must connect your phone to your laptop. Then, run:
```
npm run android
npm run start
```

The application should be installed on your phone and usable without being connected to your laptop.

## Component interrelations

The following diagram describes the interrelations between the different components.

```mermaid
graph TD
    open([Open the application])

    token-id{Token and ID stored ?}
    teacher{Is a teacher ?}

    loggin-action[Loggin]

    loggin-view(Loggin view)
    register-view(Register view)
    main-view(Main view)
    pokemons-view(Pokemons view)
    pokemon-detail-view(Pokemon detail view)
    profile-view(Profile view)
    info-view(Info view)
    students-view(Students view)

    open --> token-id

    token-id -->|Yes| loggin-action
    token-id -->|No| loggin-view

    loggin-action -->|Success| main-view
    loggin-action -->|Failed| loggin-view

    loggin-view --> loggin-action
    loggin-view --> register-view

    register-view --> loggin-view

    main-view --> pokemons-view
    pokemons-view --> main-view

    main-view --> profile-view
    profile-view --> main-view
    profile-view --> |Logout or delete account| loggin-view

    pokemons-view --> |Click on a pokemon| pokemon-detail-view
    pokemon-detail-view --> pokemons-view

    main-view --> teacher
    teacher --> |Yes| students-view
    students-view --> main-view
    teacher --> |No| info-view
    info-view --> main-view
```

## Use cases

Different use cases are possible in this application.

For each use case, only the use case where there is no error will be specified.
In case of an error, the frontend returns the same view with the appropriate error message.

```mermaid
sequenceDiagram
    User->>+Frontend: View
    User->>+Frontend: Action
    Frontend->>+Backend: Call
    Backend-->>+Frontend: Error
    Frontend-->>+User: Same view with the error message
```

### Open the application

To avoid the user to enter his credentials each time he opens the application, the token and the ID necessary for the exchanges with our backend are recorded in the internal storage of the phone thanks to [Async Storage](https://react-native-async-storage.github.io/async-storage).

Thus, each time the application is opened, the frontend will retrieve its information and try to connect the user. Depending on the response of the backend, the frontend will return the appropriate view.

```mermaid
sequenceDiagram
    User->>+Frontend: Open the application
    Frontend->>+Frontend: Get token
    Frontend->>+Frontend: Get ID
    opt Token and ID not null
        Frontend->>Backend: Get user
        alt Successfull get user
            Backend-->>+Frontend: { id: <id>, name: <name>, role: <role>, ... }
            Frontend->>+Frontend: Update user
        else Get user failed
            alt Forbidden
                Backend-->>+Frontend: 403 - FORBIDDEN
                Frontend->>+Frontend: Delete token
                Frontend->>+Frontend: Delete ID
            else Other error
                Backend-->>+Frontend: Error
                Frontend->>+Frontend: Update error
            end
        end
        Frontend->>Backend: Get seen pokemons
        Backend-->>+Frontend: [{ id: <id> }]
        Frontend->>+Frontend: Update seen pokemons
        Frontend->>Backend: Get caught pokemons
        Backend-->>+Frontend: [{ id: <id> }]
        Frontend->>+Frontend: Update caught pokemons
    end
    Frontend->>+PokeAPI: Get pokemons(page one)
    PokeAPI-->>+Frontend: { count: <number>, results: <list>, ... }
    Frontend->>+Frontend: Update pokemons
    Frontend->>+Frontend: Update nbPokemons
    alt User not null
        Frontend-->>+User: Main user
    else User is null
        Frontend-->>+User: Loggin view
    end
```

Moreover, the application constantly checks the status of the scroll view for the pokemon view in order to load the pokemons as they come in.

```mermaid
sequenceDiagram
    User->>+Frontend: Main view
    loop true
        opt Pokemon view scroll bar at the end && not all pokemons are loaded
            Frontend->>+PokeAPI: Get pokemons(page)
            PokeAPI-->>+Frontend: { count: <number>, results: <list>, ... }
            Frontend->>+Frontend: Update pokemons with the results
            Frontend->>+Frontend: Increment page
        end
    end
```

### Register

To register, the user must enter all the information necessary to create a user.
He will have to enter a `username`, a `password` (repeated twice for more security), a `gender` and a `role`.

![Register view](./img/register.jpg)

Once all the information is entered, the user can then click on the register button, which until now was not clickable.

The frontend will then make an API call to our [backend](./backend.md) to create the user.

If there are no errors, the user will be automatically loggin thanks to the previous information.

If the connection is successful, the frontend will save the received token and ID to the internal storage. As said before, the objective is to allow the user to login automatically.

```mermaid
sequenceDiagram
    User->>+Frontend: Register view
    User->>+Frontend: Enter a username
    User->>+Frontend: Enter a password
    User->>+Frontend: Repeat the password
    User->>+Frontend: Choose a gender
    User->>+Frontend: Choose a role
    Frontend->>+Backend: Create the user
    Backend-->>+Frontend: 201 - CREATED
    Frontend->>+Backend: Login
    Backend-->>+Frontend: { id: <id>, token: <token> }
    Frontend->>+Frontend: Update token
    Frontend->>+Frontend: Update ID
    Frontend->>Backend: Get user
    Backend-->>+Frontend: { id: <id>, name: <name>, role: <role>, ... }
    Frontend->>+Frontend: Update user
    Frontend->>Backend: Get seen pokemons
    Backend-->>+Frontend: [{ id: <id> }]
    Frontend->>+Frontend: Update seen pokemons
    Frontend->>Backend: Get caught pokemons
    Backend-->>+Frontend: [{ id: <id> }]
    Frontend->>+Frontend: Update caught pokemons
    Frontend-->>+User: Main view
```

### Login

The behavior is similar to the register use case.

If the login is good then the information will be saved in the phone's internal storage.

![Login view](./img/login.jpg)

```mermaid
sequenceDiagram
    User->>+Frontend: Loggin view
    User->>+Frontend: Enter the username
    User->>+Frontend: Enter the password
    Frontend->>+Backend: Login
    Backend-->>+Frontend: { id: <id>, token: <token> }
    Frontend->>+Frontend: Update token
    Frontend->>+Frontend: Update ID
    Frontend->>Backend: Get user
    Backend-->>+Frontend: { id: <id>, name: <name>, role: <role>, ... }
    Frontend->>+Frontend: Update user
    Frontend->>Backend: Get seen pokemons
    Backend-->>+Frontend: [{ id: <id> }]
    Frontend->>+Frontend: Update seen pokemons
    Frontend->>Backend: Get caught pokemons
    Backend-->>+Frontend: [{ id: <id> }]
    Frontend->>+Frontend: Update caught pokemons
    Frontend-->>+User: Main view
```

### Main view

Once logged in, the user has access to the main view which provides several entry points depending on what he wants to do.

#### Pokemons

This view doesn't call any API.

![Pokemons view](./img/pokemons.jpg)

```mermaid
sequenceDiagram
    User->>+Frontend: Pokemons view
    Frontend-->>+User: Pokemons view
```

#### Pokemon detail

By clicking on a pokemon it is possible to access the details view.

This view adapts the information displayed according to the state of the pokemon (`unknown`, `seen`, `caught`) in the right language (here English).

From this view it is possible to change the status of a pokemon, from `unknown` to `seen`, and from `seen` to `caught`.

```mermaid
sequenceDiagram
    User->>+Frontend: Pokemons view
    Frontend->>Backend: Get seen pokemons
    Backend-->>+Frontend: [{ id: <id> }]
    Frontend->>+Frontend: Update seen pokemons
    Frontend->>Backend: Get caught pokemons
    Backend-->>+Frontend: [{ id: <id> }]
    Frontend->>+Frontend: Update caught pokemons
    Frontend-->>+User: Pokemons view with the list of pokemons
    User->>+Frontend: Click on a pokemon
    Frontend->>+Frontend: Pokemon detail view
    Frontend->>+PokeAPI: Get pokemon
    PokeAPI-->>+Frontend: { id: <id>, name: <name>, ... }
    Frontend->>+PokeAPI: Get pokemon specie
    PokeAPI-->>+Frontend: { names: [language: { name: <name> }], flavor_text_entries: [language: { name: <name> }, flavor_text: <description>], ... }
    Frontend->>+Frontend: Update pokemon
    Frontend-->>+User: Pokemon detail view with the pokemon information
```

##### Seen pokemon

To change the status of the pokemon from `unknown` to `seen`, just click on the button that appears.

```mermaid
sequenceDiagram
    User->>+Frontend: Pokemon detail view with the pokemon information
    User->>+Frontend: Click on the seen button
    Frontend->>+Backend: Add seen pokemon
    Backend-->>+Frontend: [{ id: <id> }]
    Frontend-->>+User: Pokemon detail view with the pokemon information updated
```

##### Caught pokemon

To change the status of the pokemon from `seen` to `caught`, just click on the button that appears.

```mermaid
sequenceDiagram
    User->>+Frontend: Pokemon detail view with the pokemon information
    User->>+Frontend: Click on the caught button
    Frontend->>+Backend: Add caught pokemon
    Backend-->>+Frontend: [{ id: <id> }]
    Frontend-->>+User: Pokemon detail view with the pokemon information updated
```

#### Students

If the user is a teacher he can have access to the view allowing him to manage his students.

In this view he can see his students, add or delete them.

![Students view](./img/students.jpg)

##### Display students

When opening the view, the frontend will automatically call our API to retrieve the list of students and display them.

```mermaid
sequenceDiagram
    User->>+Frontend: Students view
    Frontend->>+Backend: Get list of students
    Backend-->>+Frontend: 200 - OK
    Frontend-->>+User: Students view with the list of students
```

##### Add a student

In order to add a student, the user must enter the name of the student to be added.

If the student's name is correct, the student is added to the list of students and displayed in the view.

```mermaid
sequenceDiagram
    User->>+Frontend: Students view with the list of students
    User->>+Frontend: Enter a student name
    User->>+Frontend: Add a student
    Frontend->>+Backend: Add a student
    Backend-->>+Frontend: 200 - OK
    Frontend-->>+User: Students view with the list of students
```

##### Remove a student

In order to delete a student, the user must click on the trash icon next to the student.

If all went well, then the student is removed from the student list and is no longer displayed in the view.

```mermaid
sequenceDiagram
    User->>+Frontend: Students view with the list of students
    User->>+Frontend: Remove a student
    Frontend->>+Backend: Remove a student
    Backend-->>+Frontend: 200 - OK
    Frontend-->>+User: Students view with the list of students
```

#### Information

If the user is a student he can have access to the view allowing him to see his teacher and the number of pokemons seen or caught.

This view doesn't call any API.

![Info view](./img/infos.jpg)

```mermaid
sequenceDiagram
    User->>+Frontend: Info view
    Frontend-->>+User: Info view
```

#### Profile

In this view, the user can modify his profile, log out or delete his account.

![Profile view](./img/profile.jpg)

##### Update the user information

To modify the information of a user, the password is required.

Once the password is entered it is possible to modify any information.

A success message is displayed if everything went well.

```mermaid
sequenceDiagram
    User->>+Frontend: Profile view
    User->>+Frontend: Enter the password
    opt Update username
        User->>+Frontend: Change the username
    end
    opt Update user password
        User->>+Frontend: Change the password
        User->>+Frontend: Repeat the password
    end
    opt Update user gender
        User->>+Frontend: Change the gender
    end
    Frontend->>+Backend: Update the user
    Backend-->>+Frontend: 200 - OK
    Frontend->>+Frontend: Update user
    Frontend-->>+User: Profile view with success message
```

##### Logout

The user can log out at any time.

To do this, the frontend will delete the ID and token that was previously stored and return the user to the loggin view.

```mermaid
sequenceDiagram
    User->>+Frontend: Profile view
    User->>+Frontend: Logout
    Frontend->>+Frontend: Remove token
    Frontend->>+Frontend: Remove ID
    Frontend->>Frontend: Remove user
    Frontend-->>+User: Loggin view
```

##### Delete the user account

The user can delete his account at any time.

In case of deletion a confirmation message is displayed before the account is permanently deleted.

![Confirmation message](./img/delete.jpg)

Once the account is deleted, the frontend behavior is similar to the log out.

```mermaid
sequenceDiagram
    User->>+Frontend: Profile view
    User->>+Frontend: Delete account
    Frontend-->>+User: Are you sure ?
    alt Yes
        Frontend->>+Backend: Delete user
        Backend-->>+Frontend: 204 - NO CONTENT
        Frontend->>+Frontend: Remove token
        Frontend->>+Frontend: Remove ID
        Frontend->>Frontend: Remove user
        Frontend-->>+User: Loggin view
    else No
        Frontend-->>+User: Profile view
    end
```

## Tests

Several type of tests are available and runned by the [Gitlab](https://gitlab.com) CI.

### Audit

This test checks the [JavaScript](https://developer.mozilla.org/fr/docs/Web/JavaScript) vulnerabilities.

To run it:
```
npm audit
```

### Linting

This test checks syntax, finds problems, and enforces code style. For this we use [eslint](https://eslint.org/).

To run it:
```
npm run lint
```
