# Endpoints

You can access all these endpoints at `https://pokedex-ensimag.herokuapp.com/api/` thanks to [Heroku](https://heroku.com).

Endpoints with a `*` require a [JWT](https://jwt.io). This token is sent upon connection and it is required in the `x-access-token` header.

Parameters with a `*` are required.

## Errors

The API may return the following errors:
- 400 - `Bad request`
- 403 - `Forbidden`
- 404 - `Not found`
- 405 - `Method not allowed`
- 500 - `Internal server error`

For 400 errors an additional response is sent to provide more precision.

This message is a map with:
- a `key` indicating the subject of the error
- a `value` indicating the error message

## Users

**Note**:
- The user ID is generate automatically.
- The username cannot be longer than 16 charaters.
- The sex can only be `male`, `female` or `other`.
- The role can only be `student` or `teacher`.
- The role cannot be changed.

### Login

POST: `users/login`
Parameters: `name`\* and `password`\*

```
200 - OK
{
    id : <userId>,
    token: <token>
}
```

| Code |                  Message                   |                        Why                         |
|------|--------------------------------------------|----------------------------------------------------|
| 400  |          {name : 'Invalid name'}           |   `name` parameter not found in the body request   |
| 400  |       {password: 'Invalid password'}       | `password` parameter not found in the body request |
| 400  | {\_\_all\_\_: 'Invalid login or password'} | The user with this name and password doesn't exist |

### Create a user

POST: `users/`
Parameters: `name`\*, `password`\*, `role`\* and `sex`\*

```
201 - CREATED
{
    id: <userId>,
    name: <username>,
    role: <userRole>,
    sex: <userSex>,
    teacher: {
        id: <teacherId>,
        name: <teacherName>,
        role: <teacherRole>,
        sex: <teacherSex>
    }
}
```

| Code |            Message             |                                   Why                                     |
|------|--------------------------------|---------------------------------------------------------------------------|
| 400  |     {name : 'Invalid name'}    |              `name` parameter not found in the body request               |
| 400  | {password: 'Invalid password'} |            `password` parameter not found in the body request             |
| 400  |     {role: 'Invalid role'}     | `role` parameter not found in the body request or has not a correct value |
| 400  |      {sex: 'Invalid sex'}      | `sex` parameter not found in the body request or has not a correct value  |
| 400  |     {name: 'Name too long'}    |                    `name` is longer than 16 characters                    |
| 400  |  {name: 'Name already used'}   |                   `name` already used by an other user                    |
| 500  |     Depends of the error       |                   Database error when created the user                    |

### Retrieve a user

GET: `users/:id`\*
Parameters: -

```
200 - OK
{
    id: <userId>,
    name: <username>,
    role: <userRole>,
    sex: <userSex>,
    teacher: {
        id: <teacherId>,
        name: <teacherName>,
        role: <teacherRole>,
        sex: <teacherSex>
    }
}
```

| Code | Message |      Why      |
|------|---------|---------------|
| 403  |         | Invalid token |

### Update a user

POST: `users/:id`\*
Parameters: current `password`\*, `name`, `sex` and `newPassword`

```
200 - OK
{
    id: <userId>,
    name: <username>,
    role: <userRole>,
    sex: <userSex>,
    teacher: {
        id: <teacherId>,
        name: <teacherName>,
        role: <teacherRole>,
        sex: <teacherSex>
    }
}
```

```
304 - NOT MODIFIED
```

| Code |            Message             |                               Why                                  |
|------|--------------------------------|--------------------------------------------------------------------|
| 400  | {password: 'Invalid password'} | `password` parameter not found in the body request or is incorrect |
| 400  |      {sex: 'Invalid sex'}      |                    `sex` has not a correct value                   |
| 400  |     {name: 'Name too long'}    |                  `name` is longer than 16 characters               |
| 400  |  {name: 'Name already used'}   |                 `name` already used by an other user               |
| 403  |                                |                           Invalid token                            |
| 500  |      Depends of the error      |                Database error when created the user                |

### Delete a user

DELETE: `users/:id`\*
Parameters: -

```
204 - NO CONTENT
```

| Code | Message |      Why      |
|------|---------|---------------|
| 403  |         | Invalid token |

## Students

**Note**:
- Only teachers can access to these endpoints because only teacher can have students.
- Teachers can only add a student.

### Get a list of students

GET: `users/:id/students`\*
Parameters: -

```
200 - OK
[
    {
        id: <userId>,
        name: <username>,
        role: <userRole>,
        sex: <userSex>
    }
]
```

| Code |                     Message                      |          Why              |
|------|--------------------------------------------------|---------------------------|
| 400  | {\_\_all\_\_: 'The <username> is not a teacher'} | The user is not a teacher |
| 403  |                                                  |       Invalid token       |

### Add a student

POST: `users/:id/students`\*
Parameters: student `name`\*

```
200 - OK
[
    {
        id: <userId>,
        name: <username>,
        role: <userRole>,
        sex: <userSex>
    }
]
```

| Code |                     Message                      |                     Why                        |
|------|--------------------------------------------------|------------------------------------------------|
| 400  | {\_\_all\_\_: 'The <username> is not a teacher'} |            The user is not a teacher           |
| 400  | {\_\_all\_\_: 'The <username> is not a student'} |      The username is not a student's name      |
| 400  |              {name: 'Invalid name'}              | `name` parameter not found in the body request |
| 400  |   {name: 'This student already has a teacher'}   |       This student already has a teacher       |
| 403  |                                                  |                 Invalid token                  |
| 404  |                                                  |               Student not found                |

### Remove a student

DELETE: `users/:id/students`\*
Parameters: student `name`\*

```
200 - OK
[
    {
        id: <userId>,
        name: <username>,
        role: <userRole>,
        sex: <userSex>
    }
]
```

| Code |                     Message                      |                     Why                        |
|------|--------------------------------------------------|------------------------------------------------|
| 400  | {\_\_all\_\_: 'The <username> is not a teacher'} |            The user is not a teacher           |
| 400  | {\_\_all\_\_: 'The <username> is not a student'} |      The username is not a student's name      |
| 400  |              {name: 'Invalid name'}              | `name` parameter not found in the body request |
| 400  |   {name: 'This student already has a teacher'}   |       This student already has a teacher       |
| 403  |                                                  |                 Invalid token                  |
| 404  |                                                  |          Student not found in the list         |

## Pokemons

**Note**:
- Only students can add a pokemon (seen or caught).
- The teacher can retrieve a unique list of all the pokemons seen or caught by their students.
- A caught pokemon is automatically seen if it wasn't before.

### Retrieve the seen pokemons

GET: `users/:id/pokemons/seen`\*
Parameters: -

```
200 - OK
[
    <pokemonId>
]
```

| Code | Message |      Why      |
|------|---------|---------------|
| 403  |         | Invalid token |

### Add seen pokemons

POST: `users/:id/pokemons/seen`\*
Parameters: `pokemons`\* IDs

```
200 - OK
[
    <pokemonId>
]
```

| Code |                     Message                      |                       Why                          |
|------|--------------------------------------------------|----------------------------------------------------|
| 400  | {\_\_all\_\_: 'The <username> is not a student'} |             The user is not a student              |
| 400  |          {pokemons: 'Invalid pokemons'}          | `pokemons` parameter not found in the body request |
| 403  |                                                  |                   Invalid token                    |

### Retrieve the caught pokemons

GET: `users/:id/pokemons/caught`\*
Parameters: -

```
200 - OK
[
    <pokemonId>
]
```

| Code | Message |      Why      |
|------|---------|---------------|
| 403  |         | Invalid token |

### Add caught pokemons

POST: `users/:id/pokemons/caught`\*
Parameters: `pokemons`\* IDs

```
200 - OK
[
    <pokemonId>
]
```

| Code |                     Message                      |                       Why                          |
|------|--------------------------------------------------|----------------------------------------------------|
| 400  | {\_\_all\_\_: 'The <username> is not a student'} |             The user is not a student              |
| 400  |          {pokemons: 'Invalid pokemons'}          | `pokemons` parameter not found in the body request |
| 403  |                                                  |                   Invalid token                    |
