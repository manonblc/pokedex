# Backend

The backend manages user roles (`student` or `teacher`) and the hiereach.

It also saves the pokemons seen or caught by students.

Teachers can find all the pokemons seen or caught by their students.

**Note**: For the rest of the documentation we will consider the commands in the `backend` folder.

## Architecture

In the `backend` folder is:
- an `updatebd.js` file which allows to create and populate the database (for more information see below)
- a `Procfile` file which allows to define the command launched by [Heroku](https://heroku.com)
- a `jest.config.js` file that allows to mock the environment variables for the tests
- a `.eslintrc.yml` file that allows to define the different rules of linting
- an `src` folder with all the backend code

### Source folder

In the `src` folder is:
- a `server.js` file that runs the backend server
- a `controllers` folder split by use cases (`users`, `students`, `pokemons`, `middleware`) which contains the functions called by the server
- a `models` folder  which defines the database tables (`users`, `pokemons`, `seenPokemons`, `caughtPokemons`)
- a `routes` folder which defines the different endpoints. There is only one `users.js` file because all endpoints are relative to a user.
- a `tests` folder which contains all the tests of the `controllers`
- a `utils` folder which contains `constants` and `helpers` functions which are used by several modules.

## Installation

To install the backend dependencies, run the following commands:
```
npm install
```

## Configuration

To start your local server, you must create a `.env` file in the `backend` directory.
It is necessary to set:
- the `PORT` of the server
- the `SECRET` used to create the token
- the `ALGO` used to create the token

Since this server is local, you can use any values. For example you can run:
```
echo "PORT=1234" >> .env
echo "SECRET=Sup3rtS3cr3t" >> .env
echo "ALGO=HS256" >> .env
```

## Populate the database

To create and populate a database, run:
```
node updatedb.js
```

The database will be populated with two teachers, eight students and forty pokemon IDs.

The teacher `Sebastien` has the following students:
- `Sacha`
- `Benjamin`
- `Manon`

The teacher `Christine` has the following students:
- `Emma`
- `Morgane`
- `Enki`
- `Alex`

The student `Hugo` has no teacher.

All users have the same password: `123456`.

The pokemons seen and caught will be added randomly.

| Student  | Seen | Caught |              Details             |
|----------|------|--------|----------------------------------|
|  Sacha   |  ✓   |   ✓    | Seen and caught can be different |
| Benjamin |  ✓   |   ✗    |                                  |
|  Manon   |  ✓   |   ✓    |  Seen and caught are identical   |
|   Emma   |  ✗   |   ✗    |                                  |
| Morgane  |  ✓   |   ✓    | Seen and caught can be different |
|   Enki   |  ✓   |   ✓    |  Seen and caught are identical   |
|   Alex   |  ✓   |   ✓    | Seen and caught can be different |
|   Hugo   |  ✓   |   ✗    |                                  |

## Start a local server

To create, populate a database and start a local server, run:
```
npm start
```

## Database

The backend communicates with a [SQLite](https://www.sqlite.org) database. For this, we use the ORM language [Sequelize](https://sequelize.org).

### Model

The database schema is as follows:

```mermaid
classDiagram
    class Sexs{
        <<enumeration>>
        MALE male
        FEMALE female
        OTHER other
    }

    class Roles{
        <<enumeration>>
        TEACHER teacher
        STUDENT student
    }

    class User{
        +int id
        +String name
    }

    class Pokemon{
        +int id
    }

    SeenPokemon "*" --* "1" Pokemon
    SeenPokemon "*" --* "1" User

    User "1" <-- User: teacher
    Roles "1" <-- User: role
    Sexs "1" <-- User: sex

    Pokemon "1" *-- "*" CaughtPokemon
    User "1" *-- "*" CaughtPokemon
```

For the user role and sex we use an **enumeration**.

### Pokemon state

A pokemon can be `unknown`, `seen` or `caught` by a user.

If a pokemon is `caught` then it is necessarily `seen`.

```mermaid
stateDiagram-v2
    [*] --> Unknown
    Unknown --> Seen
    Seen --> Caught
    Caught --> [*]
```

## Endpoints

For more information see the [dedicated section](./endpoints.md)

## Security

It has had several reflections to guarantee a minimal security in our backend.

### Password

The passwords are not stored in the database in clear. We used the [bcrypt](https://www.npmjs.com/package/bcrypt) library. It allows to hash the passwords before store them in the database (either during the creation or during an update).

The passwords are **never** in the API responses. The rewriting of the `toJson` prototype of the [Sequelize](https://sequelize.org) models allows to remove this parameter before any API response.

### Token

To use the different endpoints for a specific user, it is necessary to provide a [JWT](https://jwt.io) token. To generate it, it is necessary to specify an algorithm and a secret. In our case, this information is provided by the environment variables. To avoid having a common `.env` file and especially to avoid that this file is visible in the git repository, we have used the environment variables of [Heroku](https://heroku.com).

![Heroku variables](./img/heroku_variables.png)

### Gitlab CI

We set up the automatic deployment of the backend when it is the `master` branch and it is `tagged`. This deployment requires the `api key` of our [Heroku](https://heroku.com) project. In order that this key is not accessible directly on the git repository, we used the environment variables of [Gitlab](https://gitlab.com).

![Gitlab variables](./img/gitlab_variables.png)

## Deploy

### Locally

To deploy a new version of the backend on [Heroku](https://heroku.com), you can run:
```
cd ..
git subtree push --prefix backend heroku master
```

### Using Gitlab CI

You can use the [Gitlab](https://gitlab.com) CI to deploy a version of the backend.
For this you must tag the `master` branch and push it with the commands:
```
git checkout master
git tag <version>
git push origin master <version>
```

## Tests

Several type of tests are available and runned by the [Gitlab](https://gitlab.com) CI.

### Audit

This test checks the [JavaScript](https://developer.mozilla.org/fr/docs/Web/JavaScript) vulnerabilities.

To run it:
```
npm audit
```

### Linting

This test checks syntax, finds problems, and enforces code style. For this we use [eslint](https://eslint.org/).

To run it:
```
npm run lint
```

### API

This test checks the different use cases of the API. For this we use [jest](https://jestjs.io/fr/).

To run it:
```
npm run test
```