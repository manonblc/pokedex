import React, {useEffect, useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {Text, Input, Icon, Card} from 'react-native-elements';
import User from './User';
import mainStyle from '../js/styles';
import {getStudents, addStudent, removeStudent} from '../js/api';

const Students = ({id, token, seenPokemon, caughtPokemon}) => {
  const [name, setName] = useState(null);
  const [students, setStudents] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const results = await getStudents(id, token);
        setStudents(results);
      } catch (err) {
        setError(err);
      }
    };
    setError(null);
    fetchData();
  }, [id, token]);

  const remove = async studentName => {
    setLoading(true);
    setError(null);
    try {
      const results = await removeStudent(id, token, {name: studentName});
      setStudents(results);
    } catch (err) {
      setError(err);
    }
    setLoading(false);
  };

  const add = async () => {
    if (disabled()) {
      return;
    }
    setLoading(true);
    setError(null);
    try {
      const results = await addStudent(id, token, {name});
      setName('');
      setStudents(results);
    } catch (err) {
      setError(err);
    }
    setLoading(false);
  };

  const disabled = () => {
    return !name || loading;
  };

  return (
    <View>
      {/* Error */}
      {error && error.__all__ ? (
        <Text style={mainStyle.error}>{error.__all__}</Text>
      ) : null}

      {/* Input to add a student */}
      <View style={styles.inline}>
        <Input
          disabled={loading}
          errorMessage={error && error.name ? error.name : null}
          label="Student name"
          placeholder="Name"
          leftIcon={{type: 'font-awesome', name: 'graduation-cap'}}
          value={name}
          onChangeText={setName}
          containerStyle={styles.container}
        />
        <Icon
          name="plus-circle"
          type="font-awesome"
          onPress={add}
          disabled={disabled()}
          disabledStyle={styles.disabled}
        />
      </View>

      {/* List all students */}
      {students.map(student => (
        <View style={[styles.inline, mainStyle.marginBottom]} key={student.id}>
          <User user={student} />
          <Icon
            name="trash-alt"
            type="font-awesome-5"
            onPress={() => remove(student.name)}
            disabled={loading}
            disabledStyle={styles.disabled}
          />
        </View>
      ))}
      <Card>
        <View style={styles.alignCenter}>
          <Icon name="eye-outline" type="ionicon" />
          <Text style={mainStyle.marginTop}>
            Pokemon seen : {seenPokemon.length}
          </Text>
        </View>
        <Card.Divider />
        <View style={styles.alignCenter}>
          <Icon name="pokeball" type="material-community" />
          <Text style={mainStyle.marginTop}>
            Pokemon caught : {caughtPokemon.length}
          </Text>
        </View>
      </Card>
    </View>
  );
};

const styles = StyleSheet.create({
  inline: {
    ...mainStyle.inline,
    justifyContent: 'space-between',
  },
  container: {
    width: 250,
  },
  alignCenter: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 15,
  },
  disabled: {backgroundColor: 'transparent', opacity: 0.1},
});

export default Students;
