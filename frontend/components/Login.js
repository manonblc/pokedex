import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {Button, Input, Text, Card} from 'react-native-elements';
import {login} from '../js/api';
import mainStyle from '../js/styles';

const Login = ({setToken, setId, setRegister}) => {
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const onLogin = async () => {
    setError(null);
    setLoading(true);
    try {
      const data = await login({name, password});
      setToken(data.token);
      setId(data.id);
    } catch (err) {
      setError(err);
      setName('');
      setPassword('');
      setLoading(false);
    }
  };

  const disabled = () => {
    return !name || !password || loading;
  };

  return (
    <View>
      <Card>
        <Card.Title>Login</Card.Title>
        <Card.Divider />
        {/* Error */}
        {error && error.__all__ ? (
          <Text style={[mainStyle.error, mainStyle.marginBottom]}>
            {error.__all__}
          </Text>
        ) : null}

        {/* Username */}
        <Input
          disabled={loading}
          errorMessage={error && error.name ? error.name : null}
          label="Username"
          placeholder="Name"
          leftIcon={{type: 'font-awesome', name: 'user'}}
          value={name}
          onChangeText={setName}
        />

        {/* Password */}
        <Input
          disabled={loading}
          errorMessage={error && error.password ? error.password : null}
          label="Password"
          placeholder="Password"
          leftIcon={{type: 'font-awesome', name: 'lock'}}
          value={password}
          onChangeText={setPassword}
          secureTextEntry={true}
        />

        {/* Register link */}
        <Text style={styles.link} onPress={() => setRegister(true)}>
          Register
        </Text>

        {/* Login button */}
        <Button
          title="Login"
          onPress={onLogin}
          disabled={disabled()}
          loading={loading}
        />
      </Card>
    </View>
  );
};

const styles = StyleSheet.create({
  link: {
    ...mainStyle.link,
    ...mainStyle.marginBottom,
    textAlign: 'right',
  },
});

export default Login;
