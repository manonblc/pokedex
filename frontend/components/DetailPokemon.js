import React, {useEffect, useState} from 'react';
import {View, StyleSheet, ActivityIndicator} from 'react-native';
import {fetchPokemonById} from '../js/pokeapi.js';
import {TypesIcons} from './TypesIcons';
import {Icon} from 'react-native-elements/dist/icons/Icon';
import {Button, Text, Card} from 'react-native-elements';
import {updateSeenPokemon, updateCaughtPokemon} from '../js/api.js';
import {ROLES} from '../js/constants';
import mainStyle from '../js/styles';

const DetailPokemon = ({
  id,
  token,
  user,
  setAction,
  pokemonId,
  seenPokemon,
  caughtPokemon,
  refreshSeenPokemon,
  refreshCaughtPokemon,
}) => {
  const [pokemon, setPokemon] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetchPokemonById(pokemonId)
      .then(fetchedPokemon => {
        setPokemon(fetchedPokemon);
      }, [])
      .catch(reason => setError(reason));
  }, [pokemonId]);

  useEffect(() => {
    if (pokemon !== null && pokemon !== undefined) {
      setIsLoading(false);
    }
  }, [pokemon]);

  const seePokemon = async () => {
    setIsLoading(true);
    try {
      await updateSeenPokemon(id, token, {
        pokemons: seenPokemon.concat([pokemonId]),
      });
      refreshSeenPokemon();
    } catch (err) {
      setError(err);
    }
    setIsLoading(false);
  };

  const catchPokemon = async () => {
    setIsLoading(true);
    try {
      await updateCaughtPokemon(id, token, {
        pokemons: caughtPokemon.concat([pokemonId]),
      });
      refreshCaughtPokemon();
    } catch (err) {
      setError(err);
    }
    setIsLoading(false);
  };

  const isSeen = () => {
    return (
      pokemon !== null &&
      pokemon !== undefined &&
      seenPokemon.includes(parseInt(pokemonId, 10))
    );
  };

  const isCaught = () => {
    return (
      pokemon !== null &&
      pokemon !== undefined &&
      caughtPokemon.includes(parseInt(pokemonId, 10))
    );
  };

  return (
    <View style={styles.container}>
      {/* Error */}
      {error && error ? <Text style={mainStyle.error}>{error}</Text> : null}

      <Button
        title="Return to list"
        onPress={() => setAction('Pokemons')}
        icon={<Icon name="arrow-back-ios" type="materialIcons" color="white" />}
        buttonStyle={styles.button}
      />
      {isLoading ? (
        <ActivityIndicator size="large" color="#ff0000" />
      ) : (
        <View>
          <Card style={styles.upperPart}>
            <Card.Divider />
            <Card.Title style={styles.font30}>
              n°{pokemon.id} - {pokemon.name}
            </Card.Title>
            <Card.Divider />
            <View style={styles.alignCenter}>
              {isSeen() ? (
                <Card.Image source={{uri: pokemon.img}} style={styles.img} />
              ) : (
                <Text style={styles.upperPartText}>Form : unknown</Text>
              )}
            </View>
            <Card.Divider />
            <View style={styles.alignCenter}>
              <Text style={styles.upperPartText}>
                Taille : {isCaught() ? pokemon.height + '"' : 'unknown'}
              </Text>
              <Text style={styles.upperPartText}>
                Poids : {isCaught() ? pokemon.weight + ' kg' : 'unknown'}
              </Text>
            </View>
            <Card.Divider />
            <View style={styles.alignCenter}>
              {isCaught() ? (
                <TypesIcons style={styles.types} types={pokemon.types} />
              ) : (
                <Text style={styles.name}>unknown types</Text>
              )}
            </View>
            <Card.Divider />
            <View style={styles.alignCenter}>
              <Text>
                {isCaught()
                  ? pokemon.description
                  : isSeen()
                  ? 'not caught yet'
                  : 'not seen yet'}
              </Text>
            </View>
            <Card.Divider />
          </Card>
          {!isSeen() && user.role === ROLES.STUDENT ? (
            <Button
              title="See"
              onPress={seePokemon}
              buttonStyle={styles.button}
              icon={<Icon name="eye-outline" type="ionicon" color="white" />}
            />
          ) : !isCaught() && user.role === ROLES.STUDENT ? (
            <Button
              title="Catch"
              onPress={catchPokemon}
              buttonStyle={styles.button}
              icon={
                <Icon name="pokeball" type="material-community" color="white" />
              }
            />
          ) : null}
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#2B292E',
    flex: 1,
  },
  image: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 150,
    height: 150,
  },
  upperPart: {
    backgroundColor: '#DD0000',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  upperPartText: {
    color: 'black',
  },
  font30: {
    fontSize: 30,
  },
  name: {
    fontSize: 15,
  },
  downPart: {
    margin: 10,
    padding: 10,
    backgroundColor: 'white',
  },
  alignCenter: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 15,
  },
  button: {
    backgroundColor: '#2B292E',
  },
  img: {
    width: 150,
    height: 150,
  },
});

export default DetailPokemon;
