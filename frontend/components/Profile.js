import React, {useEffect, useState} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {Input, Text, Button, Overlay, CheckBox} from 'react-native-elements';
import {SEXS} from '../js/constants';
import {updateUser, deleteUser} from '../js/api';
import mainStyle from '../js/styles';

const Profile = ({setToken, setId, setAction, setUser, token, id, user}) => {
  const initSexs = Object.keys(SEXS).map(key => {
    return {id: key, value: SEXS[key], selected: user.sex === SEXS[key]};
  });

  const errorInit = {password: 'Password is required'};

  const [name, setName] = useState(user.name);
  const [password, setPassword] = useState('');
  const [repeatPassword, setRepeatPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [sexs, setSexs] = useState(initSexs);
  const [success, setSuccess] = useState(false);
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = useState(false);
  const [error, setError] = useState(errorInit);

  useEffect(() => {
    if (!password) {
      setError(e => {
        return {...e, password: 'Password is required'};
      });
    } else {
      setError(e => {
        delete e.password;
        return e;
      });
    }
  }, [password]);

  useEffect(() => {
    if ((newPassword || repeatPassword) && newPassword !== repeatPassword) {
      setError(e => {
        return {...e, repeatPassword: 'Different passwords'};
      });
    } else {
      setError(e => {
        delete e.repeatPassword;
        return e;
      });
    }
  }, [repeatPassword, newPassword]);

  const onPress = (set, list, item) => {
    const newItems = list.map(i => {
      return {...i, selected: i.id === item.id};
    });
    set(newItems);
  };

  const onUpdate = async () => {
    setError(null);
    setLoading(true);
    try {
      const body = {name, password, newPassword, sex: getSex()};
      const userUpdated = await updateUser(id, token, body);
      if (userUpdated) {
        setUser(userUpdated);
      }
      setPassword('');
      setRepeatPassword('');
      setNewPassword('');
      setLoading(false);
      setSuccess(true);
    } catch (err) {
      setError(err);
      setLoading(false);
    }
  };

  const disabled = () => {
    return (
      !name ||
      !password ||
      ((newPassword || repeatPassword) && newPassword !== repeatPassword) ||
      !getSex() ||
      loading
    );
  };

  const getSex = () => {
    const sex = sexs.find(s => s.selected);
    return sex ? sex.value : null;
  };

  const onLogout = () => {
    setAction('Pokemons');
    setToken(null);
    setId(null);
  };

  const onDelete = async () => {
    setError(null);
    setLoading(true);
    try {
      await deleteUser(id, token);
      setLoading(false);
      onLogout();
    } catch (err) {
      setError(err);
      setLoading(false);
    }
  };

  return (
    <View>
      {/* Error */}
      {error && error.__all__ ? (
        <Text style={mainStyle.error}>{error.__all__}</Text>
      ) : null}

      {/* Success */}
      {success ? (
        <Text h4 style={mainStyle.success}>
          All updated
        </Text>
      ) : null}

      {/* Username */}
      <Input
        disabled={loading}
        errorMessage={error && error.name ? error.name : null}
        label="Username"
        placeholder="Name"
        leftIcon={{type: 'font-awesome', name: 'user'}}
        value={name}
        onChangeText={setName}
      />

      {/* Password */}
      <Input
        disabled={loading}
        errorMessage={error && error.password ? error.password : null}
        label="Password *"
        placeholder="Password"
        leftIcon={{type: 'font-awesome', name: 'lock'}}
        value={password}
        onChangeText={setPassword}
        secureTextEntry={true}
      />

      {/* New password */}
      <Input
        disabled={loading}
        errorMessage={error && error.newPassword ? error.newPassword : null}
        label="New password"
        placeholder="New password"
        leftIcon={{type: 'font-awesome', name: 'lock'}}
        value={newPassword}
        onChangeText={setNewPassword}
        secureTextEntry={true}
      />

      {/* Repeat new password */}
      <Input
        disabled={loading}
        errorMessage={
          error && error.repeatPassword ? error.repeatPassword : null
        }
        label="Repeat new password *"
        placeholder="Password"
        leftIcon={{type: 'font-awesome', name: 'lock'}}
        value={repeatPassword}
        onChangeText={setRepeatPassword}
        secureTextEntry={true}
      />

      {/* Gender */}
      {sexs.map(item => (
        <CheckBox
          disabled={loading}
          key={item.id}
          title={item.value}
          onPress={() => onPress(setSexs, sexs, item)}
          checked={item.selected}
          containerStyle={styles.container}
          textStyle={styles.text}
        />
      ))}

      <Overlay
        isVisible={visible}
        onBackdropPress={() => setVisible(false)}
        overlayStyle={styles.overlay}>
        <View>
          <Text h4 h4Style={styles.black}>
            Are you sure you want to delete your account?
          </Text>
          <Text style={styles.black}>This action is irreversible...</Text>
        </View>
        <View style={styles.buttons}>
          <TouchableOpacity>
            <Button
              icon={{type: 'entypo', name: 'circle-with-cross'}}
              title="Cancel"
              onPress={() => setVisible(false)}
              loading={loading}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <Button
              icon={{type: 'font-awesome-5', name: 'trash'}}
              title="Delete"
              onPress={onDelete}
              loading={loading}
              buttonStyle={styles.danger}
            />
          </TouchableOpacity>
        </View>
      </Overlay>

      {/* Buttons */}
      <View style={styles.buttons}>
        <TouchableOpacity>
          <Button
            icon={{type: 'font-awesome-5', name: 'trash'}}
            title="Delete"
            onPress={() => setVisible(true)}
            loading={loading}
            buttonStyle={styles.danger}
          />
        </TouchableOpacity>
        <TouchableOpacity>
          <Button
            icon={{name: 'logout'}}
            title="Logout"
            onPress={onLogout}
            loading={loading}
          />
        </TouchableOpacity>
        <TouchableOpacity>
          <Button
            icon={{name: 'edit'}}
            title="Updated"
            onPress={onUpdate}
            disabled={disabled()}
            loading={loading}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    borderWidth: 0,
  },
  text: {
    textTransform: 'capitalize',
  },
  buttons: {
    ...mainStyle.inline,
    ...mainStyle.marginTop,
    justifyContent: 'space-between',
  },
  danger: {
    backgroundColor: 'crimson',
  },
  black: {
    color: 'black',
  },
  overlay: {
    display: 'flex',
    justifyContent: 'space-between',
    width: 250,
    height: 150,
  },
});

export default Profile;
