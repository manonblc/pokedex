import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Text, Card, Icon} from 'react-native-elements';
import User from './User';
import mainStyle from '../js/styles';
import {ROLES} from '../js/constants';

const Infos = ({user, style, seenPokemon, caughtPokemon}) => {
  return (
    <View style={style}>
      {/* Teacher details */}
      {user.teacher ? (
        <User style={mainStyle.marginTop} user={user.teacher} />
      ) : null}
      {user.role === ROLES.STUDENT && !user.teacher ? (
        <Text style={mainStyle.marginTop}>You do not have a teacher yet</Text>
      ) : null}

      {/* User details */}
      <User style={mainStyle.marginTop} user={user} />

      <Card>
        <View style={styles.alignCenter}>
          <Icon name="eye-outline" type="ionicon" />
          <Text style={mainStyle.marginTop}>
            Pokemon seen : {seenPokemon.length}
          </Text>
        </View>
        <Card.Divider />
        <View style={styles.alignCenter}>
          <Icon name="pokeball" type="material-community" />
          <Text style={mainStyle.marginTop}>
            Pokemon caught : {caughtPokemon.length}
          </Text>
        </View>
      </Card>
    </View>
  );
};

const styles = StyleSheet.create({
  inline: {
    ...mainStyle.inline,
    justifyContent: 'space-between',
  },
  alignCenter: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 15,
  },
});

export default Infos;
