import React from 'react';
import {Image, View, StyleSheet} from 'react-native';
import {switchType} from '../js/pokeapi.js';
import {POKEBIP_URL} from '../js/constants';

const TypesIcons = props => {
  if (props.types.length === 0) {
    return (
      <View style={styles.container}>
        <Image
          id="image"
          style={styles.image}
          source={{uri: POKEBIP_URL + 'gen4_types/-1.png'}}
        />
      </View>
    );
  } else if (props.types.length === 1) {
    let type = switchType(props.types[0].type);
    return (
      <View style={styles.container}>
        <Image
          id="image"
          style={styles.image}
          source={{uri: POKEBIP_URL + type + '.png'}}
        />
      </View>
    );
  } else {
    let type1 = switchType(props.types[0].type);
    let type2 = switchType(props.types[1].type);
    return (
      <View style={styles.container}>
        <Image
          id="image"
          style={styles.image}
          source={{uri: POKEBIP_URL + type1 + '.png'}}
        />
        <Image
          id="image"
          style={styles.image}
          source={{uri: POKEBIP_URL + type2 + '.png'}}
        />
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  image: {
    margin: 10,
    width: 75,
    height: 25,
  },
});

export {TypesIcons};
