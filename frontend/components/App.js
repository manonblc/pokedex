import React, {useEffect, useState} from 'react';
import {
  View,
  ScrollView,
  SafeAreaView,
  StyleSheet,
  useColorScheme,
} from 'react-native';
import {Text, ThemeProvider} from 'react-native-elements';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Login from './Login';
import Register from './Register';
import Header from './Header';
import Infos from './Infos';
import Pokemons from './Pokemons';
import DetailPokemon from './DetailPokemon';
import Profile from './Profile';
import Students from './Students';
import mainStyle from '../js/styles';
import {getUser, getSeenPokemon, getCaughtPokemon} from '../js/api';
import {getPokemons} from '../js/pokeapi';
import {FORBIDDEN, PAGINATION} from '../js/constants';

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const [token, setToken] = useState(null);
  const [id, setId] = useState(null);
  const [user, setUser] = useState(null);
  const [action, setAction] = useState('Pokemons');
  const [error, setError] = useState(null);
  const [register, setRegister] = useState(false);
  const [seenPokemon, setSeenPokemon] = useState([]);
  const [caughtPokemon, setCaughtPokemon] = useState([]);
  const [pokemons, setPokemons] = useState([]);
  const [nbPokemons, setNbPokemons] = useState(null);
  const [page, setPage] = useState(1);
  const [pokemonId, setPokemonId] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const tokenStored = await AsyncStorage.getItem('@user_token');
      const idStored = await AsyncStorage.getItem('@user_id');
      setToken(tokenStored);
      setId(idStored);
      try {
        const pkms = await getPokemons(0);
        setNbPokemons(pkms.count);
        setPokemons(pkms.results);
        setPage(p => p + 1);
      } catch (err) {
        setError(err);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    const updateStorage = async () => {
      // Synchronize local storage
      const map = {token, id};
      for (const key in map) {
        const item = `@user_${key}`;
        if (map[key]) {
          await AsyncStorage.setItem(item, String(map[key]));
        } else {
          await AsyncStorage.removeItem(item);
        }
      }
    };

    const updateUser = async () => {
      // Get user
      try {
        const data = token && id ? await getUser(id, token) : null;
        setUser(data);
      } catch (err) {
        setUser(null);
        if (err === FORBIDDEN) {
          setToken(null);
          setId(null);
        } else {
          setError(err);
        }
      }
    };

    setError(null);
    updateStorage();
    updateUser();
    refreshSeenPokemon();
    refreshCaughtPokemon();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token, id]);

  useEffect(() => {
    if (action !== 'Pokemons') {
      return;
    }
    refreshSeenPokemon();
    refreshCaughtPokemon();
    setPokemons(p => p.slice(0, PAGINATION));
    setPage(2);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [action]);

  const refreshSeenPokemon = async () => {
    // Get seen Pokemon
    try {
      const data = token && id ? await getSeenPokemon(id, token) : [];
      setSeenPokemon(data);
    } catch (err) {
      setError(err);
    }
  };

  const refreshCaughtPokemon = async () => {
    // Get seen Pokemon
    try {
      const data = token && id ? await getCaughtPokemon(id, token) : [];
      setCaughtPokemon(data);
    } catch (err) {
      setError(err);
    }
  };

  const loadPokemons = async nativeEvent => {
    if (
      action !== 'Pokemons' ||
      (nativeEvent && !isCloseToBottom(nativeEvent)) ||
      lastPage()
    ) {
      return;
    }
    setError(null);
    try {
      const pkms = await getPokemons(page - 1);
      setPokemons(p => p.concat(pkms.results));
      setPage(p => p + 1);
    } catch (err) {
      setError(err);
    }
  };

  const isLogged = () => {
    return user;
  };

  const lastPage = () => {
    return page > Math.ceil(nbPokemons / PAGINATION);
  };

  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height;
  };

  return (
    <SafeAreaView style={mainStyle.fullHeight}>
      <ThemeProvider useDark={isDarkMode}>
        {/* Error */}
        {error && error.__all__ ? (
          <Text style={mainStyle.error}>{error.__all__}</Text>
        ) : null}

        {isLogged() ? (
          <View style={styles.flex}>
            <Header setAction={setAction} user={user} />
            <ScrollView
              style={mainStyle.padding}
              onScroll={({nativeEvent}) => {
                loadPokemons(nativeEvent);
              }}>
              {action === 'DetailPokemon' ? (
                <DetailPokemon
                  style={mainStyle.fullHeight}
                  id={id}
                  token={token}
                  user={user}
                  setAction={setAction}
                  pokemonId={pokemonId}
                  seenPokemon={seenPokemon}
                  caughtPokemon={caughtPokemon}
                  refreshSeenPokemon={refreshSeenPokemon}
                  refreshCaughtPokemon={refreshCaughtPokemon}
                />
              ) : null}
              {action === 'Infos' ? (
                <Infos
                  style={mainStyle.marginBottom}
                  user={user}
                  seenPokemon={seenPokemon}
                  caughtPokemon={caughtPokemon}
                />
              ) : null}
              {action === 'Pokemons' ? (
                <Pokemons
                  setAction={setAction}
                  setPokemonId={setPokemonId}
                  pokemons={pokemons}
                  seenPokemon={seenPokemon}
                  caughtPokemon={caughtPokemon}
                  lastPage={lastPage()}
                />
              ) : null}
              {action === 'Profile' ? (
                <Profile
                  setToken={setToken}
                  setId={setId}
                  setAction={setAction}
                  setUser={setUser}
                  token={token}
                  id={id}
                  user={user}
                />
              ) : null}
              {action === 'Students' ? (
                <Students
                  token={token}
                  id={id}
                  seenPokemon={seenPokemon}
                  caughtPokemon={caughtPokemon}
                />
              ) : null}
            </ScrollView>
          </View>
        ) : register ? (
          <ScrollView
            contentContainerStyle={[
              mainStyle.centerVertically,
              styles.flexGrow,
            ]}>
            <Register
              setToken={setToken}
              setId={setId}
              setRegister={setRegister}
            />
          </ScrollView>
        ) : (
          <ScrollView
            contentContainerStyle={[
              mainStyle.centerVertically,
              styles.flexGrow,
            ]}>
            <Login
              setToken={setToken}
              setId={setId}
              setRegister={setRegister}
            />
          </ScrollView>
        )}
      </ThemeProvider>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  flex: {
    flex: 1,
    height: '100%',
  },
  flexGrow: {
    flexGrow: 1,
  },
});

export default App;
