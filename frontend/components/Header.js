import React, {useState} from 'react';
import {View, useColorScheme} from 'react-native';
import {Tab} from 'react-native-elements';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {ROLES} from '../js/constants';

const Header = ({setAction, user}) => {
  const isDarkMode = useColorScheme() === 'dark';
  const color = isDarkMode ? Colors.white : Colors.black;

  const [value, setValue] = useState(0);

  const items = [
    {
      title: 'Pokemons',
      icon: {type: 'material-community', name: 'pokeball'},
      titleStyle: {color},
      containerStyle: {backgroundColor: 'transparent'},
    },
    {
      title: 'Profile',
      icon: {type: 'font-awesome', name: 'user'},
      titleStyle: {color},
      containerStyle: {backgroundColor: 'transparent'},
    },
  ];
  if (user.role === ROLES.STUDENT) {
    const infos = {
      title: 'Infos',
      icon: {type: 'material-icons', name: 'info'},
      titleStyle: {color},
      containerStyle: {backgroundColor: 'transparent'},
    };
    items.splice(1, 0, infos);
  }
  if (user.role === ROLES.TEACHER) {
    const student = {
      title: 'Students',
      icon: {type: 'font-awesome', name: 'graduation-cap'},
      titleStyle: {color},
      containerStyle: {backgroundColor: 'transparent'},
    };
    items.splice(1, 0, student);
  }

  const onChange = newValue => {
    const action = items[newValue].title;
    setValue(newValue);
    setAction(action);
  };

  return (
    <View>
      {/* Menu */}
      <Tab
        indicatorStyle={{backgroundColor: color}}
        value={value}
        onChange={onChange}>
        {items.map(item => (
          <Tab.Item key={item.title} {...item} />
        ))}
      </Tab>
    </View>
  );
};

export default Header;
