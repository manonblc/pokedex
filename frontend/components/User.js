import React from 'react';
import {View} from 'react-native';
import {Avatar, Text} from 'react-native-elements';
import mainStyle from '../js/styles';
import {ROLES, SEXS} from '../js/constants';

const User = ({user, style}) => {
  const images = {
    [SEXS.FEMALE]: {
      [ROLES.STUDENT]: require('../img/student_female.png'),
      [ROLES.TEACHER]: require('../img/teacher_female.png'),
    },
    [SEXS.MALE]: {
      [ROLES.STUDENT]: require('../img/student_male.png'),
      [ROLES.TEACHER]: require('../img/teacher_male.png'),
    },
  };

  const source = () => {
    if (user.sex === SEXS.OTHER) {
      return;
    }
    return images[user.sex][user.role];
  };

  return (
    <View style={[mainStyle.inline, style]}>
      <Avatar
        size="medium"
        rounded
        source={source()}
        icon={{type: 'font-awesome', name: 'user'}}
      />
      <Text h4 style={mainStyle.marginLeft}>
        {user.name}
      </Text>
    </View>
  );
};

export default User;
