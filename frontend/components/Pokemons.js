import React from 'react';
import {
  View,
  ActivityIndicator,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';
import {Text, Icon} from 'react-native-elements';
import mainStyle from '../js/styles';

const Pokemons = ({
  setAction,
  setPokemonId,
  pokemons,
  seenPokemon,
  caughtPokemon,
  lastPage,
}) => {
  return (
    <View style={mainStyle.marginBottom}>
      {/* Pokemons */}
      {pokemons.map((item, index) => (
        <View key={index} style={styles.item}>
          <TouchableHighlight
            onPress={() => {
              // Display detailPokemon with the right index
              setPokemonId(index + 1);
              setAction('DetailPokemon');
            }}
            style={styles.button}>
            <View style={styles.buttonView}>
              {seenPokemon.includes(index + 1) ? (
                <Icon style={styles.icon} name="eye-outline" type="ionicon" />
              ) : null}
              <Text style={styles.pokemon}>
                {index + 1} - {item.name}
              </Text>
              {caughtPokemon.includes(index + 1) ? (
                <Icon
                  style={styles.icon}
                  name="pokeball"
                  type="material-community"
                />
              ) : null}
            </View>
          </TouchableHighlight>
        </View>
      ))}

      {/* Loading indicator */}
      {!lastPage ? <ActivityIndicator size="large" color="red" /> : null}
    </View>
  );
};

const styles = StyleSheet.create({
  pokemon: {
    flex: 1,
    padding: 15,
    fontSize: 23,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#000000',
  },
  button: {
    backgroundColor: '#BB4444',
    margin: 5,
    borderRadius: 50,
  },
  buttonView: {
    flexDirection: 'row',
  },
  icon: {
    margin: 18,
  },
  inline: {
    ...mainStyle.inline,
    justifyContent: 'space-between',
  },
  inputSize: {
    width: 100,
  },
});

export default Pokemons;
