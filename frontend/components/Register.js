import React, {useEffect, useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {Text, Card, Input, CheckBox, Button} from 'react-native-elements';
import {createUser, login} from '../js/api';
import {ROLES, SEXS} from '../js/constants';
import mainStyle from '../js/styles';

const Register = ({setToken, setId, setRegister}) => {
  const initError = {
    name: 'Name is required',
    password: 'Password is required',
  };
  const initRoles = Object.keys(ROLES).map(id => {
    return {id, value: ROLES[id], selected: false};
  });
  const initSexs = Object.keys(SEXS).map(id => {
    return {id, value: SEXS[id], selected: false};
  });

  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [repeatPassword, setRepeatPassword] = useState('');
  const [roles, setRoles] = useState(initRoles);
  const [sexs, setSexs] = useState(initSexs);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(initError);

  const onPress = (set, list, item) => {
    const newItems = list.map(i => {
      return {...i, selected: i.id === item.id};
    });
    set(newItems);
  };

  const onRegister = async () => {
    setError(null);
    setLoading(true);
    try {
      await createUser({name, password, role: getRole(), sex: getSex()});
      const data = await login({name, password});
      setRegister(false);
      setToken(data.token);
      setId(data.id);
    } catch (err) {
      setError(err);
      setLoading(false);
    }
  };

  useEffect(() => {
    if (!name) {
      setError(e => {
        return {...e, name: 'Name is required'};
      });
    } else {
      setError(e => {
        delete e.name;
        return e;
      });
    }
  }, [name]);

  useEffect(() => {
    if (!password) {
      setError(e => {
        return {...e, password: 'Password is required'};
      });
    } else {
      setError(e => {
        delete e.password;
        return e;
      });
    }

    if ((password || repeatPassword) && password !== repeatPassword) {
      setError(e => {
        return {...e, repeatPassword: 'Different passwords'};
      });
    } else {
      setError(e => {
        delete e.repeatPassword;
        return e;
      });
    }
  }, [password, repeatPassword]);

  const disabled = () => {
    return (
      !name ||
      !password ||
      !repeatPassword ||
      password !== repeatPassword ||
      !getSex() ||
      !getRole() ||
      loading
    );
  };

  const getSex = () => {
    const sex = sexs.find(s => s.selected);
    return sex ? sex.value : null;
  };

  const getRole = () => {
    const role = roles.find(r => r.selected);
    return role ? role.value : null;
  };

  return (
    <View>
      <Card>
        <Card.Title>Register</Card.Title>
        <Card.Divider />
        {/* Error */}
        {error && error.__all__ ? (
          <Text style={[mainStyle.error, mainStyle.marginBottom]}>
            {error.__all__}
          </Text>
        ) : null}

        {/* Username */}
        <Input
          disabled={loading}
          errorMessage={error && error.name ? error.name : null}
          label="Username *"
          placeholder="Name"
          leftIcon={{type: 'font-awesome', name: 'user'}}
          value={name}
          onChangeText={setName}
        />

        {/* Password */}
        <Input
          disabled={loading}
          errorMessage={error && error.password ? error.password : null}
          label="Password *"
          placeholder="Password"
          leftIcon={{type: 'font-awesome', name: 'lock'}}
          value={password}
          onChangeText={setPassword}
          secureTextEntry={true}
        />

        {/* Repeat password */}
        <Input
          disabled={loading}
          errorMessage={
            error && error.repeatPassword ? error.repeatPassword : null
          }
          label="Repeat password *"
          placeholder="Password"
          leftIcon={{type: 'font-awesome', name: 'lock'}}
          value={repeatPassword}
          onChangeText={setRepeatPassword}
          secureTextEntry={true}
        />

        <Card.Divider />

        {/* Gender */}
        {sexs.map(item => (
          <CheckBox
            disabled={loading}
            key={item.id}
            title={item.value}
            onPress={() => onPress(setSexs, sexs, item)}
            checked={item.selected}
            containerStyle={styles.container}
            textStyle={styles.text}
          />
        ))}

        <Card.Divider />

        {/* Role */}
        {roles.map(item => (
          <CheckBox
            disabled={loading}
            key={item.id}
            title={item.value}
            onPress={() => onPress(setRoles, roles, item)}
            checked={item.selected}
            containerStyle={styles.container}
            textStyle={styles.text}
          />
        ))}

        {/* Sign in link */}
        <Text style={styles.link} onPress={() => setRegister(false)}>
          Login
        </Text>

        {/* Login button */}
        <Button
          title="Register"
          onPress={onRegister}
          disabled={disabled()}
          loading={loading}
        />
      </Card>
    </View>
  );
};

const styles = StyleSheet.create({
  link: {
    ...mainStyle.link,
    ...mainStyle.marginBottom,
    textAlign: 'right',
  },
  container: {
    backgroundColor: 'transparent',
    borderWidth: 0,
  },
  text: {
    textTransform: 'capitalize',
  },
});

export default Register;
