import {StyleSheet} from 'react-native';

const mainStyle = StyleSheet.create({
  error: {
    textAlign: 'center',
    color: 'red',
  },
  success: {
    textAlign: 'center',
    color: 'green',
  },
  padding: {
    padding: 10,
  },
  marginBottom: {
    marginBottom: 10,
  },
  marginTop: {
    marginTop: 10,
  },
  marginLeft: {
    marginLeft: 10,
  },
  fullHeight: {
    height: '100%',
  },
  centerVertically: {
    display: 'flex',
    justifyContent: 'center',
  },
  inline: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  link: {
    textDecorationLine: 'underline',
  },
});

export default mainStyle;
