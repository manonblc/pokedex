import {BACKEND_URL} from './constants';
import {checkErrors} from './utils';

export const login = async body => {
  const response = await fetch(`${BACKEND_URL}/users/login`, {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(body),
  });
  return await checkErrors(response);
};

// User
export const createUser = async body => {
  const response = await fetch(`${BACKEND_URL}/users`, {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(body),
  });
  return await checkErrors(response);
};

export const getUser = async (id, token) => {
  const response = await fetch(`${BACKEND_URL}/users/${id}`, {
    headers: {
      'Content-Type': 'application/json',
      'x-access-token': token,
    },
  });
  return await checkErrors(response);
};

export const updateUser = async (id, token, body) => {
  const response = await fetch(`${BACKEND_URL}/users/${id}`, {
    method: 'POST',
    headers: {'Content-Type': 'application/json', 'x-access-token': token},
    body: JSON.stringify(body),
  });
  return await checkErrors(response);
};

export const deleteUser = async (id, token) => {
  const response = await fetch(`${BACKEND_URL}/users/${id}`, {
    method: 'DELETE',
    headers: {'Content-Type': 'application/json', 'x-access-token': token},
  });
  return await checkErrors(response);
};

// Students
export const getStudents = async (id, token) => {
  const response = await fetch(`${BACKEND_URL}/users/${id}/students`, {
    headers: {
      'Content-Type': 'application/json',
      'x-access-token': token,
    },
  });
  return await checkErrors(response);
};

export const addStudent = async (id, token, body) => {
  const response = await fetch(`${BACKEND_URL}/users/${id}/students`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-access-token': token,
    },
    body: JSON.stringify(body),
  });
  return await checkErrors(response);
};

export const removeStudent = async (id, token, body) => {
  const response = await fetch(`${BACKEND_URL}/users/${id}/students`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      'x-access-token': token,
    },
    body: JSON.stringify(body),
  });
  return await checkErrors(response);
};

export const getSeenPokemon = async (id, token) => {
  const response = await fetch(`${BACKEND_URL}/users/${id}/pokemons/seen`, {
    headers: {
      'Content-Type': 'application/json',
      'x-access-token': token,
    },
  });
  return await checkErrors(response);
};

export const updateSeenPokemon = async (id, token, pokemons) => {
  const response = await fetch(`${BACKEND_URL}/users/${id}/pokemons/seen`, {
    method: 'POST',
    headers: {'Content-Type': 'application/json', 'x-access-token': token},
    body: JSON.stringify(pokemons),
  });
  return await checkErrors(response);
};

export const getCaughtPokemon = async (id, token) => {
  const response = await fetch(`${BACKEND_URL}/users/${id}/pokemons/caught`, {
    headers: {
      'Content-Type': 'application/json',
      'x-access-token': token,
    },
  });
  return await checkErrors(response);
};

export const updateCaughtPokemon = async (id, token, pokemons) => {
  const response = await fetch(`${BACKEND_URL}/users/${id}/pokemons/caught`, {
    method: 'POST',
    headers: {'Content-Type': 'application/json', 'x-access-token': token},
    body: JSON.stringify(pokemons),
  });
  return await checkErrors(response);
};
