export const ROLES = {
  STUDENT: 'student',
  TEACHER: 'teacher',
};

export const SEXS = {
  MALE: 'male',
  FEMALE: 'female',
  OTHER: 'other',
};

export const BACKEND_URL = 'https://pokedex-ensimag.herokuapp.com/api';
export const POKEAPI_URL = 'https://pokeapi.co/api/v2';
export const POKEBIP_URL = 'http://www.pokebip.com/pokemon/pokedex/images/';

export const FORBIDDEN = {__all__: 'Invalid token'};
export const NOT_FOUND = {__all__: 'Not found'};
export const METHOD_NOT_ALLOWED = {__all__: 'Method not allowed'};
export const DEFAULT_ERROR = {__all__: 'Something went wrong...'};

export const PAGINATION = 20;
