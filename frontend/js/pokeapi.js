import {POKEAPI_URL, PAGINATION} from '../js/constants';
import {checkErrors} from './utils';

let language = 'en'; // fr pour langue francaise

const getPokemons = async page => {
  const offset = page * PAGINATION;
  const url = POKEAPI_URL + `/pokemon?limit=${PAGINATION}&offset=${offset}`;
  const response = await fetch(url);
  return await checkErrors(response);
};

async function fetchPokemonById(id) {
  // On récupère les informations générales du pokémon
  let url = POKEAPI_URL + '/pokemon/' + id;
  let generalResponse = await fetch(url).catch(reason => {
    throw Error(reason);
  });
  let pokemon = null;
  if (generalResponse !== undefined && generalResponse !== null) {
    let parsedGeneralResponse = await generalResponse.json();
    pokemon = {
      id: id,
      name: parsedGeneralResponse.name, // le nom est en anglais et sera remplacé par la suite s'il est trouvé dans la langue de l'application
      img: parsedGeneralResponse.sprites.front_default,
      height: parsedGeneralResponse.height,
      weight: parsedGeneralResponse.weight,
      types: parsedGeneralResponse.types,
    };

    // On récupère les informations liées à son espèce
    let specieInformationsResponse = await fetch(
      parsedGeneralResponse.species.url,
    ).catch(reason => {
      throw Error(reason);
    });
    if (
      specieInformationsResponse !== undefined &&
      specieInformationsResponse !== null
    ) {
      let parsedSpecieInformationsResponse =
        await specieInformationsResponse.json();

      // On récupère les informations dans la bonne langue
      let name = await parsedSpecieInformationsResponse.names.find(
        fetchedName => fetchedName.language.name === language,
      ).name;
      let description =
        await parsedSpecieInformationsResponse.flavor_text_entries.find(
          fetchedDescription => fetchedDescription.language.name === language,
        );
      description =
        description !== null &&
        description !== undefined &&
        description.flavor_text !== null &&
        description.flavor_text !== undefined
          ? description.flavor_text
          : null;

      // On complète l'objet relatif au pokémon
      pokemon.name = name;
      pokemon.description = description;
    }
  }
  return pokemon;
}

function switchType(type) {
  switch (type.name) {
    case 'steel':
      return 'Acier';
    case 'fighting':
      return 'Combat';
    case 'dragon':
      return 'Dragon';
    case 'water':
      return 'Eau';
    case 'electric':
      return 'Electrik';
    case 'fire':
      return 'Feu';
    case 'fairy':
      return 'gen6_types/18'; //type fée
    case 'ice':
      return 'Glace';
    case 'bug':
      return 'Insecte';
    case 'normal':
      return 'Normal';
    case 'grass':
      return 'Plante';
    case 'poison':
      return 'Poison';
    case 'psychic':
      return 'Psy';
    case 'rock':
      return 'Roche';
    case 'ground':
      return 'Sol';
    case 'ghost':
      return 'Spectre';
    case 'dark':
      return 'Tenebres';
    case 'flying':
      return 'Vol';
  }
}

export {getPokemons, fetchPokemonById, switchType};
