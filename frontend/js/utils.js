import {
  FORBIDDEN,
  NOT_FOUND,
  METHOD_NOT_ALLOWED,
  DEFAULT_ERROR,
} from './constants';

export const checkErrors = async response => {
  if (response.status === 204 || response.status === 304) {
    return;
  }
  if (response.status === 403) {
    throw FORBIDDEN;
  }
  if (response.status === 404) {
    throw NOT_FOUND;
  }
  if (response.status === 405) {
    throw METHOD_NOT_ALLOWED;
  }
  if (response.status > 400 && response.status <= 500) {
    throw DEFAULT_ERROR;
  }
  const result = await response.json();
  if (response.status === 400) {
    throw result;
  }
  return result;
};
